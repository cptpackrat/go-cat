package cat

import (
	"bytes"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"os"
	"path"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type encodeChainCase struct {
	name string
	fn   func(ch Chain, wk bool) ([]byte, error)
}

type decodeChainCase struct {
	name     string
	withKey  bool
	withRest bool
}

type issueChainCase struct {
	name string
	tu   *x509.Certificate
	fn   func(ca Chain, ts []*Template) (Chain, error)
}

func (c encodeChainCase) String() string { return c.name }
func (c decodeChainCase) String() string { return c.name }
func (c issueChainCase) String() string  { return c.name }

func TestGenerateChain(t *testing.T) {
	t.Parallel()
	testGenerateChain(t, tuChain, GenerateChain)
}

func TestGenerateClientChain(t *testing.T) {
	t.Parallel()
	testGenerateChain(t, tuClient, GenerateClientChain)
}

func TestGenerateServerChain(t *testing.T) {
	t.Parallel()
	testGenerateChain(t, tuServer, GenerateServerChain)
}

func testGenerateChain(t *testing.T, tu *x509.Certificate, fn func(tmps []*Template) (Chain, error)) {
	testParallel(t, "IssueNone", func(t *testing.T) {
		ch, err := fn([]*Template{})
		assert.NoError(t, err)
		assert.Len(t, ch, 0)
	})
	testParallel(t, "IssueOne", func(t *testing.T) {
		tmp := Template{
			KeyFormat: RSA_2048,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test"},
			},
		}
		ch, err := fn([]*Template{&tmp})
		assert.NoError(t, err)
		assert.Len(t, ch, 1)
		assert.Equal(t, RSA_2048, ch[0].Key.Format)
		assert.True(t, compareNames(t, &tmp.Certificate, &tmp.Certificate, ch[0].Cert))
		assert.True(t, compareConstraints(t, tu, ch[0].Cert))
	})
	testParallel(t, "IssueMany", func(t *testing.T) {
		tmps := []*Template{{
			KeyFormat: RSA_2048,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 1"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 2"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 3"},
			},
		}}
		ch, err := fn(tmps)
		assert.NoError(t, err)
		assert.Len(t, ch, 3)
		assert.Equal(t, RSA_2048, ch[0].Key.Format)
		assert.Equal(t, RSA_2048, ch[1].Key.Format)
		assert.Equal(t, RSA_2048, ch[2].Key.Format)
		assert.True(t, compareNames(t, &tmps[2].Certificate, &tmps[1].Certificate, ch[0].Cert))
		assert.True(t, compareNames(t, &tmps[1].Certificate, &tmps[0].Certificate, ch[1].Cert))
		assert.True(t, compareNames(t, &tmps[0].Certificate, &tmps[0].Certificate, ch[2].Cert))
		assert.True(t, compareConstraints(t, tu, ch[0].Cert))
		assert.True(t, compareConstraints(t, tuChain, ch[1].Cert))
		assert.True(t, compareConstraints(t, tuChain, ch[2].Cert))
	})
	testParallel(t, "DefaultSerialNumber", func(t *testing.T) {
		ch, err := fn([]*Template{{
			KeyFormat: RSA_2048,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 1"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 2"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 3"},
			},
		}})
		assert.NoError(t, err)
		assert.Equal(t, big.NewInt(3), ch[0].Cert.SerialNumber)
		assert.Equal(t, big.NewInt(2), ch[1].Cert.SerialNumber)
		assert.Equal(t, big.NewInt(1), ch[2].Cert.SerialNumber)
	})
	testParallel(t, "InheritedSerialNumber", func(t *testing.T) {
		ch, err := fn([]*Template{{
			KeyFormat: RSA_2048,
			Certificate: x509.Certificate{
				SerialNumber: big.NewInt(100),
				Subject:      pkix.Name{CommonName: "Test 1"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 2"},
			},
		}, {
			Certificate: x509.Certificate{
				SerialNumber: big.NewInt(200),
				Subject:      pkix.Name{CommonName: "Test 3"},
			},
		}})
		assert.NoError(t, err)
		assert.Equal(t, big.NewInt(200), ch[0].Cert.SerialNumber)
		assert.Equal(t, big.NewInt(101), ch[1].Cert.SerialNumber)
		assert.Equal(t, big.NewInt(100), ch[2].Cert.SerialNumber)
	})
	testParallel(t, "DefaultValidityPeriod", func(t *testing.T) {
		ch, err := fn([]*Template{{
			KeyFormat: RSA_2048,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 1"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 2"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 3"},
			},
		}})
		assert.NoError(t, err)
		assert.WithinDuration(t, time.Now(), ch[0].Cert.NotBefore, time.Minute)
		assert.WithinDuration(t, time.Now().Add(time.Hour*24), ch[0].Cert.NotAfter, time.Minute)
		assert.Equal(t, ch[0].Cert.NotBefore, ch[1].Cert.NotBefore)
		assert.Equal(t, ch[0].Cert.NotAfter, ch[1].Cert.NotAfter)
		assert.Equal(t, ch[0].Cert.NotBefore, ch[2].Cert.NotBefore)
		assert.Equal(t, ch[0].Cert.NotAfter, ch[2].Cert.NotAfter)
	})
	testParallel(t, "InheritedValidityPeriod", func(t *testing.T) {
		nb1 := ParseTimeP(time.RFC3339, "2020-01-01T12:00:00Z")
		na1 := ParseTimeP(time.RFC3339, "2050-01-01T12:00:00Z")
		nb2 := ParseTimeP(time.RFC3339, "2020-02-01T12:00:00Z")
		na2 := ParseTimeP(time.RFC3339, "2050-02-01T12:00:00Z")
		ch, err := fn([]*Template{{
			KeyFormat: RSA_2048,
			Certificate: x509.Certificate{
				NotBefore: nb1,
				NotAfter:  na1,
				Subject:   pkix.Name{CommonName: "Test 1"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 2"},
			},
		}, {
			Certificate: x509.Certificate{
				NotBefore: nb2,
				NotAfter:  na2,
				Subject:   pkix.Name{CommonName: "Test 3"},
			},
		}})
		assert.NoError(t, err)
		assert.Equal(t, nb2, ch[0].Cert.NotBefore)
		assert.Equal(t, na2, ch[0].Cert.NotAfter)
		assert.Equal(t, nb1, ch[1].Cert.NotBefore)
		assert.Equal(t, na1, ch[1].Cert.NotAfter)
		assert.Equal(t, nb1, ch[2].Cert.NotBefore)
		assert.Equal(t, na1, ch[2].Cert.NotAfter)
	})
	testParallel(t, "InheritedKeyFormat", func(t *testing.T) {
		key, err := GenerateKey(RSA_2048)
		assert.NoError(t, err)
		ch, err := fn([]*Template{{
			Key: key,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 1"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 2"},
			},
		}, {
			KeyFormat: ECDSA_P384,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 3"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 4"},
			},
		}, {
			Key: key,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 5"},
			},
		}})
		assert.NoError(t, err)
		assert.Equal(t, RSA_2048, ch[0].Key.Format)
		assert.Equal(t, ECDSA_P384, ch[1].Key.Format)
		assert.Equal(t, ECDSA_P384, ch[2].Key.Format)
		assert.Equal(t, RSA_2048, ch[3].Key.Format)
		assert.Equal(t, RSA_2048, ch[4].Key.Format)
		assert.True(t, ch[0].Key.Equal(key))
		assert.True(t, ch[4].Key.Equal(key))
		assert.False(t, ch[0].Key.Equal(ch[1].Key))
		assert.False(t, ch[1].Key.Equal(ch[2].Key))
		assert.False(t, ch[2].Key.Equal(ch[3].Key))
		assert.False(t, ch[3].Key.Equal(ch[4].Key))
	})
	testParallel(t, "SpecifiedPathLength", func(t *testing.T) {
		ch, err := fn([]*Template{{
			KeyFormat: RSA_2048,
			Certificate: x509.Certificate{
				Subject:    pkix.Name{CommonName: "Test 1"},
				MaxPathLen: -1,
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 2"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject:    pkix.Name{CommonName: "Test 3"},
				MaxPathLen: 2,
			},
		}, {
			Certificate: x509.Certificate{
				Subject:    pkix.Name{CommonName: "Test 4"},
				MaxPathLen: 1,
			},
		}, {
			Certificate: x509.Certificate{
				Subject:        pkix.Name{CommonName: "Test 5"},
				MaxPathLen:     0,
				MaxPathLenZero: true,
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 6"},
			},
		}})
		assert.NoError(t, err)
		assert.True(t, compareConstraints(t, tu, ch[0].Cert))
		assert.Equal(t, true, ch[1].Cert.MaxPathLenZero)
		assert.Equal(t, 0, ch[1].Cert.MaxPathLen)
		assert.Equal(t, 1, ch[2].Cert.MaxPathLen)
		assert.Equal(t, 2, ch[3].Cert.MaxPathLen)
		assert.Equal(t, -1, ch[4].Cert.MaxPathLen)
		assert.Equal(t, -1, ch[5].Cert.MaxPathLen)
	})
}

func TestGenerateChainP(t *testing.T) {
	t.Parallel()
	testGenerateChainP(t, GenerateChainP)
}

func TestGenerateClientChainP(t *testing.T) {
	t.Parallel()
	testGenerateChainP(t, GenerateClientChainP)
}

func TestGenerateServerChainP(t *testing.T) {
	t.Parallel()
	testGenerateChainP(t, GenerateServerChainP)
}

func testGenerateChainP(t *testing.T, fn func(tmps []*Template) Chain) {
	assert.Len(t, fn([]*Template{{
		KeyFormat: ECDSA_P384_PKCS8,
		Certificate: x509.Certificate{
			Subject: pkix.Name{CommonName: "Test 1"},
		},
	}, {
		Certificate: x509.Certificate{
			Subject: pkix.Name{CommonName: "Test 2"},
		},
	}}), 2)
	assert.Panics(t, func() {
		fn([]*Template{{
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 1"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 2"},
			},
		}})
	})
}

func TestDecodeChain(t *testing.T) {
	t.Parallel()
	testDecodeChain(t, DecodeChain)
}

func TestDecodeChainP(t *testing.T) {
	t.Parallel()
	c1 := GenerateChainP([]*Template{{
		KeyFormat:   ECDSA_P384_PKCS8,
		Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test"}},
	}})
	c2 := DecodeChainP(c1.EncodeP(true), nil)
	assert.True(t, c2[0].Key.Equal(c1[0].Key))
	assert.True(t, c2[0].Cert.Equal(c1[0].Cert))
	assert.Panics(t, func() {
		DecodeChainP([]byte(""), nil)
	})
}

func TestReadChainFile(t *testing.T) {
	t.Parallel()
	testDecodeChain(t, func(data, pass []byte) (Chain, error) {
		tmp := path.Join(t.TempDir(), "test.pem")
		err := os.WriteFile(tmp, data, 0600)
		if err != nil {
			return nil, err
		}
		return ReadChainFile(tmp, pass)
	})
}

func TestReadChainFileP(t *testing.T) {
	t.Parallel()
	tmp := path.Join(t.TempDir(), "test.pem")
	c1 := GenerateChainP([]*Template{{
		KeyFormat:   ECDSA_P384_PKCS8,
		Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test"}},
	}})
	c1.WriteFileP(tmp, 0600, true)
	c2 := ReadChainFileP(tmp, nil)
	assert.True(t, c2[0].Key.Equal(c1[0].Key))
	assert.True(t, c2[0].Cert.Equal(c1[0].Cert))
	assert.Panics(t, func() {
		ReadChainFileP(tmp+".nope", nil)
	})
}

func testDecodeChain(t *testing.T, fn func(data, pass []byte) (Chain, error)) {
	testParallelForEach(t, []KeyFormat{
		RSA_2048_PKCS1,
		RSA_2048_PKCS8,
		ECDSA_P256_PKCS8,
		ECDSA_P256_SEC1,
		ED25519_PKCS8,
	}, func(t *testing.T, kf KeyFormat) {
		c1 := GenerateChainP([]*Template{{
			KeyFormat:   ECDSA_P384_PKCS8,
			Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test 1"}},
		}, {
			Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test 2"}},
		}, {
			Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test 3"}},
		}})
		testParallelForEach(t, []decodeChainCase{
			{"ChainOnly", true, false},
			{"ChainOnlyNoKey", false, false},
			{"MixedData", true, true},
			{"MixedDataNoKey", false, true},
		}, func(t *testing.T, c decodeChainCase) {
			pem, err := c1.Encode(c.withKey)
			assert.NoError(t, err)
			if c.withRest {
				pem = []byte(fmt.Sprintf("other data\n%sother data\n", pem))
			}
			c2, err := fn(pem, nil)
			assert.NoError(t, err)
			assert.Len(t, c2, 3)
			for i, p := range c2 {
				assert.True(t, p.Cert.Equal(c1[i].Cert))
				if i == 0 && c.withKey {
					assert.True(t, p.Key.Equal(c1[i].Key))
				} else {
					assert.Nil(t, p.Key)
				}
			}
		})
		testParallel(t, "EncryptedKey", func(t *testing.T) {
			pem, err := c1.Encrypt(x509.PEMCipherAES256, []byte("password"))
			assert.NoError(t, err)
			_, err = fn(pem, nil)
			assert.ErrorContains(t, err, "cannot decrypt private key without passphrase")
			c2, err := fn(pem, []byte("password"))
			assert.NoError(t, err)
			for i, p := range c2 {
				assert.True(t, p.Cert.Equal(c1[i].Cert))
				if i == 0 {
					assert.True(t, p.Key.Equal(c1[i].Key))
				} else {
					assert.Nil(t, p.Key)
				}
			}
		})
	})
	testParallel(t, "NoChain", func(t *testing.T) {
		key := GenerateKeyP(ECDSA_P384_PKCS8).EncodeP()
		_, err := fn(key, nil)
		assert.ErrorContains(t, err, "no certificate found")
		_, err = fn([]byte(""), nil)
		assert.ErrorContains(t, err, "no certificate found")
		_, err = fn([]byte("other data"), nil)
		assert.ErrorContains(t, err, "no certificate found")
		_, err = fn([]byte("-----BEGIN NOT A KEY-----\nbm90IGEga2V5Cg==\n-----END NOT A KEY-----\n"), nil)
		assert.ErrorContains(t, err, "cannot decode NOT A KEY as key")
		_, err = fn([]byte(fmt.Sprintf("%s-----BEGIN NOT A CERT-----\nbm90IGEga2V5Cg==\n-----END NOT A CERT-----\n", key)), nil)
		assert.ErrorContains(t, err, "cannot decode NOT A CERT as certificate")
	})
}

func TestChain(t *testing.T) {
	t.Parallel()
	testParallelForEach(t, []issueChainCase{
		{"IssueChain", tuChain, func(ca Chain, ts []*Template) (Chain, error) {
			return ca.IssueChain(ts)
		}},
		{"IssueClientChain", tuClient, func(ca Chain, ts []*Template) (Chain, error) {
			return ca.IssueClientChain(ts)
		}},
		{"IssueServerChain", tuServer, func(ca Chain, ts []*Template) (Chain, error) {
			return ca.IssueServerChain(ts)
		}},
	}, func(t *testing.T, c issueChainCase) {
		ca, err := GenerateChain([]*Template{{
			KeyFormat: RSA_2048,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test Root"},
			},
		}})
		assert.NoError(t, err)
		testParallel(t, "IssueNone", func(t *testing.T) {
			ch, err := c.fn(ca, []*Template{})
			assert.NoError(t, err)
			assert.Len(t, ch, 1)
		})
		testParallel(t, "IssueOne", func(t *testing.T) {
			tmps := []*Template{{
				Certificate: x509.Certificate{
					Subject: pkix.Name{CommonName: "Test"},
				},
			}}
			ch, err := c.fn(ca, tmps)
			assert.NoError(t, err)
			assert.Len(t, ch, 2)
			assert.Equal(t, RSA_2048, ch[0].Key.Format)
			assert.Equal(t, RSA_2048, ch[1].Key.Format)
			assert.True(t, compareNames(t, &tmps[0].Certificate, ca[0].Cert, ch[0].Cert))
			assert.True(t, compareNames(t, ca[0].Cert, ca[0].Cert, ch[1].Cert))
			assert.True(t, compareConstraints(t, c.tu, ch[0].Cert))
			assert.True(t, compareConstraints(t, tuChain, ch[1].Cert))
		})
		testParallel(t, "IssueMany", func(t *testing.T) {
			tmps := []*Template{{
				Certificate: x509.Certificate{
					Subject: pkix.Name{CommonName: "Test 1"},
				},
			}, {
				Certificate: x509.Certificate{
					Subject: pkix.Name{CommonName: "Test 2"},
				},
			}}
			ch, err := c.fn(ca, tmps)
			assert.NoError(t, err)
			assert.Len(t, ch, 3)
			assert.Equal(t, RSA_2048, ch[0].Key.Format)
			assert.Equal(t, RSA_2048, ch[1].Key.Format)
			assert.Equal(t, RSA_2048, ch[2].Key.Format)
			assert.True(t, compareNames(t, &tmps[1].Certificate, &tmps[0].Certificate, ch[0].Cert))
			assert.True(t, compareNames(t, &tmps[0].Certificate, ca[0].Cert, ch[1].Cert))
			assert.True(t, compareNames(t, ca[0].Cert, ca[0].Cert, ch[2].Cert))
			assert.True(t, compareConstraints(t, c.tu, ch[0].Cert))
			assert.True(t, compareConstraints(t, tuChain, ch[1].Cert))
			assert.True(t, compareConstraints(t, tuChain, ch[2].Cert))
		})
	})
	testParallelForEach(t, []issueChainCase{
		{"IssueChainP", tuChain, func(ca Chain, ts []*Template) (Chain, error) {
			return ca.IssueChainP(ts), nil
		}},
		{"IssueClientChainP", tuClient, func(ca Chain, ts []*Template) (Chain, error) {
			return ca.IssueClientChainP(ts), nil
		}},
		{"IssueServerChain", tuServer, func(ca Chain, ts []*Template) (Chain, error) {
			return ca.IssueServerChainP(ts), nil
		}},
	}, func(t *testing.T, c issueChainCase) {
		ca, err := GenerateChain([]*Template{{
			KeyFormat: RSA_2048,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test Root"},
			},
		}})
		assert.NoError(t, err)
		ch, _ := c.fn(ca, []*Template{{
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 1"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 2"},
			},
		}})
		assert.Len(t, ch, 3)
		assert.Panics(t, func() {
			c.fn(ca, []*Template{{
				KeyFormat: 1,
				Certificate: x509.Certificate{
					Subject: pkix.Name{CommonName: "Test 1"},
				},
			}, {
				Certificate: x509.Certificate{
					Subject: pkix.Name{CommonName: "Test 2"},
				},
			}})
		})
	})
	testParallelForEach(t, []KeyFormat{
		RSA_2048_PKCS1,
		RSA_3072_PKCS8,
		ECDSA_P256_SEC1,
		ECDSA_P384_PKCS8,
		ED25519_PKCS8,
	}, func(t *testing.T, kf KeyFormat) {
		ch, err := GenerateChain([]*Template{{
			KeyFormat: kf,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 1"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 2"},
			},
		}, {
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test 3"},
			},
		}})
		assert.NoError(t, err)
		assert.Len(t, ch, 3)
		testParallelForEach(t, []encodeChainCase{
			{"Encode", func(ch Chain, withKey bool) ([]byte, error) {
				return ch.Encode(withKey)
			}},
			{"Write", func(ch Chain, withKey bool) ([]byte, error) {
				buf := bytes.Buffer{}
				err := ch.Write(&buf, withKey)
				return buf.Bytes(), err
			}},
			{"WriteFile", func(ch Chain, withKey bool) ([]byte, error) {
				tmp := path.Join(t.TempDir(), "test.key")
				err := ch.WriteFile(tmp, 0600, withKey)
				if err != nil {
					return nil, err
				}
				return os.ReadFile(tmp)
			}},
		}, func(t *testing.T, c encodeChainCase) {
			testParallel(t, "WithKey", func(t *testing.T) {
				raw, err := c.fn(ch, true)
				assert.NoError(t, err)
				blk, raw := pem.Decode(raw)
				assert.NotNil(t, blk)
				key, err := tryDecodeKey(t, kf, blk)
				assert.NoError(t, err)
				assert.True(t, ch[0].Key.Equal(key))
				for _, p := range ch {
					blk, raw = pem.Decode(raw)
					assert.NotNil(t, blk)
					assert.Equal(t, "CERTIFICATE", blk.Type)
					crt, err := x509.ParseCertificate(blk.Bytes)
					assert.NoError(t, err)
					assert.True(t, p.Cert.Equal(crt))
				}
			})
			testParallel(t, "WithoutKey", func(t *testing.T) {
				raw, err := c.fn(ch, false)
				assert.NoError(t, err)
				var blk *pem.Block
				for _, p := range ch {
					blk, raw = pem.Decode(raw)
					assert.NotNil(t, blk)
					assert.Equal(t, "CERTIFICATE", blk.Type)
					crt, err := x509.ParseCertificate(blk.Bytes)
					assert.NoError(t, err)
					assert.True(t, p.Cert.Equal(crt))
				}
			})
		})
		testParallelForEach(t, []encodeChainCase{
			{"Encrypt", func(ch Chain, _ bool) ([]byte, error) {
				return ch.Encrypt(x509.PEMCipherAES256, []byte("password"))
			}},
			{"WriteEncrypted", func(ch Chain, _ bool) ([]byte, error) {
				buf := bytes.Buffer{}
				err := ch.WriteEncrypted(&buf, x509.PEMCipherAES256, []byte("password"))
				return buf.Bytes(), err
			}},
			{"WriteEncryptedFile", func(ch Chain, _ bool) ([]byte, error) {
				tmp := path.Join(t.TempDir(), "test.key")
				err := ch.WriteEncryptedFile(tmp, 0600, x509.PEMCipherAES256, []byte("password"))
				if err != nil {
					return nil, err
				}
				return os.ReadFile(tmp)
			}},
		}, func(t *testing.T, c encodeChainCase) {
			raw, err := c.fn(ch, true)
			assert.NoError(t, err)
			blk, raw := pem.Decode(raw)
			assert.NotNil(t, blk)
			key, err := tryDecryptKey(t, kf, blk, []byte("password"))
			assert.NoError(t, err)
			assert.True(t, ch[0].Key.Equal(key))
			for _, p := range ch {
				blk, raw = pem.Decode(raw)
				assert.NotNil(t, blk)
				assert.Equal(t, "CERTIFICATE", blk.Type)
				crt, err := x509.ParseCertificate(blk.Bytes)
				assert.NoError(t, err)
				assert.True(t, p.Cert.Equal(crt))
			}
		})
	})
	testParallelForEach(t, []encodeChainCase{
		{"EncodeP", func(ch Chain, withKey bool) ([]byte, error) {
			return ch.EncodeP(withKey), nil
		}},
		{"EncryptP", func(ch Chain, _ bool) ([]byte, error) {
			return ch.EncryptP(x509.PEMCipherAES256, []byte("password")), nil
		}},
		{"WriteP", func(ch Chain, withKey bool) ([]byte, error) {
			buf := bytes.Buffer{}
			ch.WriteP(&buf, withKey)
			return buf.Bytes(), nil
		}},
		{"WriteEncryptedP", func(ch Chain, _ bool) ([]byte, error) {
			buf := bytes.Buffer{}
			ch.WriteEncryptedP(&buf, x509.PEMCipherAES256, []byte("password"))
			return buf.Bytes(), nil
		}},
		{"WriteFileP", func(ch Chain, withKey bool) ([]byte, error) {
			tmp := path.Join(t.TempDir(), "test.key")
			ch.WriteFileP(tmp, 0600, withKey)
			return os.ReadFile(tmp)
		}},
		{"WriteEncryptedFileP", func(ch Chain, _ bool) ([]byte, error) {
			tmp := path.Join(t.TempDir(), "test.key")
			ch.WriteEncryptedFileP(tmp, 0600, x509.PEMCipherAES256, []byte("password"))
			return os.ReadFile(tmp)
		}},
	}, func(t *testing.T, c encodeChainCase) {
		ch, err := GenerateChain([]*Template{{
			KeyFormat: ECDSA_P384_PKCS8,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test"},
			},
		}})
		assert.NoError(t, err)
		assert.Len(t, ch, 1)
		pem, err := c.fn(ch, true)
		assert.NoError(t, err)
		assert.Greater(t, len(pem), 0)
		ch[0].Key.Format = ECDSA_P384
		assert.Panics(t, func() {
			c.fn(ch, true)
		})
	})
	{
		ch, err := GenerateChain([]*Template{{
			KeyFormat:   RSA_2048,
			Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test 1"}},
		}, {
			Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test 2"}},
		}, {
			Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test 3"}},
		}})
		assert.NoError(t, err)
		assert.Len(t, ch, 3)
		testParallel(t, "Head", func(t *testing.T) {
			assert.Equal(t, ch[:0], ch.Head(0))
			assert.Equal(t, ch[:1], ch.Head(1))
			assert.Equal(t, ch[:2], ch.Head(2))
			assert.Equal(t, ch[:3], ch.Head(3))
			assert.Equal(t, ch[:2], ch.Head(-1))
			assert.Equal(t, ch[:1], ch.Head(-2))
			assert.Equal(t, ch[:0], ch.Head(-3))
		})
		testParallel(t, "Tail", func(t *testing.T) {
			assert.Equal(t, ch[3:], ch.Tail(0))
			assert.Equal(t, ch[2:], ch.Tail(1))
			assert.Equal(t, ch[1:], ch.Tail(2))
			assert.Equal(t, ch[0:], ch.Tail(3))
			assert.Equal(t, ch[1:], ch.Tail(-1))
			assert.Equal(t, ch[2:], ch.Tail(-2))
			assert.Equal(t, ch[3:], ch.Tail(-3))
		})
		testParallel(t, "X509", func(t *testing.T) {
			crts := ch.X509()
			assert.Len(t, crts, 3)
			assert.True(t, crts[0].Equal(ch[0].Cert))
			assert.True(t, crts[1].Equal(ch[1].Cert))
			assert.True(t, crts[2].Equal(ch[2].Cert))
		})
		testParallel(t, "TLS", func(t *testing.T) {
			crt := ch.TLS()
			assert.True(t, crt.Leaf.Equal(ch[0].Cert))
			assert.Len(t, crt.Certificate, 3)
			assert.Equal(t, crt.Certificate[0], ch[0].Cert.Raw)
			assert.Equal(t, crt.Certificate[1], ch[1].Cert.Raw)
			assert.Equal(t, crt.Certificate[2], ch[2].Cert.Raw)
			assert.PanicsWithError(t, "empty chain", func() {
				Chain{}.TLS()
			})
		})
		testParallel(t, "CertPool", func(t *testing.T) {
			pool := ch.CertPool()
			//lint:ignore SA1019 subjects needed for testing
			subs := pool.Subjects()
			assert.Len(t, subs, 1)
			assert.Equal(t, subs[0], ch[2].Cert.RawSubject)
			assert.PanicsWithError(t, "empty chain", func() {
				Chain{}.CertPool()
			})
		})
		testParallel(t, "SelfSigned", func(t *testing.T) {
			assert.True(t, ch.SelfSigned())
			assert.True(t, ch[2:].SelfSigned())
			assert.False(t, ch[:2].SelfSigned())
			assert.PanicsWithError(t, "empty chain", func() {
				Chain{}.SelfSigned()
			})
		})
	}
}

func compareNames(t *testing.T, sub, iss, rcv *x509.Certificate) bool {
	return assert.Equal(t, sub.Subject.ToRDNSequence(), rcv.Subject.ToRDNSequence()) &&
		assert.Equal(t, iss.Subject.ToRDNSequence(), rcv.Issuer.ToRDNSequence())
}

func compareConstraints(t *testing.T, exp, rcv *x509.Certificate) bool {
	return assert.Equal(t, exp.KeyUsage, rcv.KeyUsage) &&
		assert.Equal(t, exp.ExtKeyUsage, rcv.ExtKeyUsage) &&
		assert.Equal(t, exp.IsCA, rcv.IsCA) &&
		assert.Equal(t, exp.MaxPathLen, rcv.MaxPathLen) &&
		assert.Equal(t, exp.MaxPathLenZero, rcv.MaxPathLenZero) &&
		assert.Equal(t, exp.BasicConstraintsValid, rcv.BasicConstraintsValid)
}
