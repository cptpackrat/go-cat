package cat

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.step.sm/crypto/pemutil"
)

type encodeCase struct {
	name string
	fn   func(key *Key) ([]byte, error)
}

func (c encodeCase) String() string { return c.name }

func TestGenerateKey(t *testing.T) {
	t.Parallel()
	key, err := GenerateKey(ECDSA_P384)
	assert.NoError(t, err)
	assert.Equal(t, ECDSA_P384, key.Format)
	_, err = GenerateKey(0)
	assert.ErrorContains(t, err, "unknown key format")
}

func TestGenerateKeyP(t *testing.T) {
	t.Parallel()
	key := GenerateKeyP(ECDSA_P384)
	assert.Equal(t, ECDSA_P384, key.Format)
	assert.Panics(t, func() {
		GenerateKeyP(0)
	})
}

func TestDecodeKey(t *testing.T) {
	t.Parallel()
	testDecodeKey(t, true, DecodeKey)
}

func TestDecodeKeyP(t *testing.T) {
	t.Parallel()
	k1 := GenerateKeyP(ECDSA_P384_PKCS8)
	k2, _ := DecodeKeyP(k1.EncodeP(), nil)
	assert.True(t, k2.Equal(k1))
	assert.Equal(t, k2.Format, k1.Format)
	assert.Panics(t, func() {
		DecodeKeyP([]byte(""), nil)
	})
}

func TestReadKeyFile(t *testing.T) {
	t.Parallel()
	testDecodeKey(t, false, func(data, pass []byte) (*Key, []byte, error) {
		tmp := path.Join(t.TempDir(), "test.key")
		err := os.WriteFile(tmp, data, 0600)
		if err != nil {
			return nil, nil, err
		}
		key, err := ReadKeyFile(tmp, pass)
		return key, nil, err
	})
}

func TestReadKeyFileP(t *testing.T) {
	t.Parallel()
	tmp := path.Join(t.TempDir(), "test.key")
	k1 := GenerateKeyP(ECDSA_P384_PKCS8)
	k1.WriteFileP(tmp, 0600)
	k2 := ReadKeyFileP(tmp, nil)
	assert.True(t, k2.Equal(k1))
	assert.Equal(t, k2.Format, k1.Format)
	assert.Panics(t, func() {
		ReadKeyFileP(tmp+".nope", nil)
	})
}

func testDecodeKey(t *testing.T, withRest bool, fn func(data, pass []byte) (*Key, []byte, error)) {
	testParallelForEach(t, []KeyFormat{
		RSA_1024_PKCS1, RSA_1024_PKCS8,
		RSA_2048_PKCS1, RSA_2048_PKCS8,
		RSA_3072_PKCS1, RSA_3072_PKCS8,
		RSA_4096_PKCS1, RSA_4096_PKCS8,
		ECDSA_P224_PKCS8, ECDSA_P224_SEC1,
		ECDSA_P256_PKCS8, ECDSA_P256_SEC1,
		ECDSA_P384_PKCS8, ECDSA_P384_SEC1,
		ECDSA_P521_PKCS8, ECDSA_P521_SEC1,
		ED25519_PKCS8,
	}, func(t *testing.T, kf KeyFormat) {
		k1, err := GenerateKey(kf)
		assert.NoError(t, err)
		p1, err := k1.Encode()
		assert.NoError(t, err)
		testParallel(t, "KeyOnly", func(t *testing.T) {
			k2, rest, err := fn(p1, nil)
			assert.NoError(t, err)
			assert.True(t, k2.Equal(k1))
			assert.Equal(t, kf, k2.Format)
			if withRest {
				assert.Equal(t, []byte(""), rest)
			}
		})
		testParallel(t, "MixedData", func(t *testing.T) {
			p2 := []byte(fmt.Sprintf("other data\n%sother data\n", p1))
			k2, rest, err := fn(p2, nil)
			assert.NoError(t, err)
			assert.True(t, k2.Equal(k1))
			assert.Equal(t, kf, k2.Format)
			if withRest {
				assert.Equal(t, []byte("other data\n"), rest)
			}
		})
		testParallel(t, "Encrypted", func(t *testing.T) {
			p2, err := k1.Encrypt(x509.PEMCipherAES256, []byte("password"))
			assert.NoError(t, err)
			_, _, err = fn(p2, nil)
			assert.ErrorContains(t, err, "cannot decrypt private key without passphrase")
			k2, _, err := fn(p2, []byte("password"))
			assert.NoError(t, err)
			assert.True(t, k2.Equal(k1))
			assert.Equal(t, kf, k2.Format)
		})
	})
	testParallel(t, "NoKey", func(t *testing.T) {
		_, _, err := fn([]byte(""), nil)
		assert.ErrorContains(t, err, "no key found")
		_, _, err = fn([]byte("other data"), nil)
		assert.ErrorContains(t, err, "no key found")
		_, _, err = fn([]byte("-----BEGIN NOT A KEY-----\nbm90IGEga2V5Cg==\n-----END NOT A KEY-----\n"), nil)
		assert.ErrorContains(t, err, "cannot decode NOT A KEY as key")
	})
}

func TestKey(t *testing.T) {
	t.Parallel()
	testParallelForEach(t, []KeyFormat{
		RSA_1024,
		RSA_2048,
		RSA_3072,
		RSA_4096,
		ECDSA_P224,
		ECDSA_P256,
		ECDSA_P384,
		ECDSA_P521,
		ED25519,
	}, func(t *testing.T, kt KeyFormat) {
		k, err := GenerateKey(kt)
		assert.NoError(t, err)
		assert.Equal(t, kt, k.Format)
		testParallelForEach(t, []KeyFormat{
			PKCS1,
			PKCS8,
			SEC1,
		}, func(t *testing.T, ke KeyFormat) {
			k1 := &Key{
				Key:    k.Key,
				Format: kt | ke,
			}
			testParallelForEach(t, []encodeCase{
				{"Encode", func(key *Key) ([]byte, error) {
					return key.Encode()
				}},
				{"Write", func(key *Key) ([]byte, error) {
					buf := bytes.Buffer{}
					err := key.Write(&buf)
					return buf.Bytes(), err
				}},
				{"WriteFile", func(key *Key) ([]byte, error) {
					tmp := path.Join(t.TempDir(), "test.key")
					err := key.WriteFile(tmp, 0600)
					if err != nil {
						return nil, err
					}
					return os.ReadFile(tmp)
				}},
			}, func(t *testing.T, c encodeCase) {
				raw, err := c.fn(k1)
				if (ke == PKCS1 && kt&RSA == 0) || (ke == SEC1 && kt&ECDSA == 0) {
					assert.ErrorContains(t, err, "unsupported key type")
				} else {
					assert.NoError(t, err)
					blk, _ := pem.Decode(raw)
					k2, err := tryDecodeKey(t, ke, blk)
					assert.NoError(t, err)
					assert.True(t, k1.Equal(k2))
				}
			})
			testParallelForEach(t, []encodeCase{
				{"Encrypt", func(key *Key) ([]byte, error) {
					return key.Encrypt(x509.PEMCipherAES256, []byte("password"))
				}},
				{"WriteEncrypted", func(key *Key) ([]byte, error) {
					buf := bytes.Buffer{}
					err := key.WriteEncrypted(&buf, x509.PEMCipherAES256, []byte("password"))
					return buf.Bytes(), err
				}},
				{"WriteEncryptedFile", func(key *Key) ([]byte, error) {
					tmp := path.Join(t.TempDir(), "test.key")
					err := key.WriteEncryptedFile(tmp, 0600, x509.PEMCipherAES256, []byte("password"))
					if err != nil {
						return nil, err
					}
					return os.ReadFile(tmp)
				}},
			}, func(t *testing.T, c encodeCase) {
				raw, err := c.fn(k1)
				if (ke == PKCS1 && kt&RSA == 0) || (ke == SEC1 && kt&ECDSA == 0) {
					assert.ErrorContains(t, err, "unsupported key type")
				} else {
					assert.NoError(t, err)
					blk, _ := pem.Decode(raw)
					k2, err := tryDecryptKey(t, ke, blk, []byte("password"))
					assert.NoError(t, err)
					assert.True(t, k1.Equal(k2))
				}
			})
			testParallelForEach(t, []encodeCase{
				{"EncodePublic", func(key *Key) ([]byte, error) {
					return key.EncodePublic()
				}},
				{"WritePublic", func(key *Key) ([]byte, error) {
					buf := bytes.Buffer{}
					err := key.WritePublic(&buf)
					return buf.Bytes(), err
				}},
				{"WritePublicFile", func(key *Key) ([]byte, error) {
					tmp := path.Join(t.TempDir(), "test.key")
					err := key.WritePublicFile(tmp, 0600)
					if err != nil {
						return nil, err
					}
					return os.ReadFile(tmp)
				}},
			}, func(t *testing.T, c encodeCase) {
				raw, err := c.fn(k1)
				if (ke == PKCS1 && kt&RSA == 0) || (ke == SEC1 && kt&ECDSA == 0) {
					assert.ErrorContains(t, err, "unsupported key type")
				} else {
					assert.NoError(t, err)
					blk, _ := pem.Decode(raw)
					p2, err := tryDecodePublicKey(t, ke, blk)
					assert.NoError(t, err)
					assert.True(t, p2.(interface {
						Equal(x crypto.PublicKey) bool
					}).Equal(k1.Public()))
				}
			})
		})
		testParallelForEach(t, []encodeCase{
			{"EncodeP", func(key *Key) ([]byte, error) {
				return key.EncodeP(), nil
			}},
			{"EncodePublicP", func(key *Key) ([]byte, error) {
				return key.EncodePublicP(), nil
			}},
			{"EncryptP", func(key *Key) ([]byte, error) {
				return key.EncryptP(x509.PEMCipherAES256, []byte("password")), nil
			}},
			{"WriteP", func(key *Key) ([]byte, error) {
				buf := bytes.Buffer{}
				key.WriteP(&buf)
				return buf.Bytes(), nil
			}},
			{"WriteEncryptedP", func(key *Key) ([]byte, error) {
				buf := bytes.Buffer{}
				key.WriteEncryptedP(&buf, x509.PEMCipherAES256, []byte("password"))
				return buf.Bytes(), nil
			}},
			{"WritePublicP", func(key *Key) ([]byte, error) {
				buf := bytes.Buffer{}
				key.WritePublicP(&buf)
				return buf.Bytes(), nil
			}},
			{"WriteFileP", func(key *Key) ([]byte, error) {
				tmp := path.Join(t.TempDir(), "test.key")
				key.WriteFileP(tmp, 0600)
				return os.ReadFile(tmp)
			}},
			{"WriteEncryptedFileP", func(key *Key) ([]byte, error) {
				tmp := path.Join(t.TempDir(), "test.key")
				key.WriteEncryptedFileP(tmp, 0600, x509.PEMCipherAES256, []byte("password"))
				return os.ReadFile(tmp)
			}},
			{"WritePublicFileP", func(key *Key) ([]byte, error) {
				tmp := path.Join(t.TempDir(), "test.key")
				key.WritePublicFileP(tmp, 0600)
				return os.ReadFile(tmp)
			}},
		}, func(t *testing.T, c encodeCase) {
			key, err := GenerateKey(ECDSA_P384_PKCS8)
			assert.NoError(t, err)
			pem, err := c.fn(key)
			assert.NoError(t, err)
			assert.Greater(t, len(pem), 0)
			key.Format = ECDSA_P384
			assert.Panics(t, func() {
				c.fn(key)
			})
		})
		k2, err := GenerateKey(kt)
		assert.NoError(t, err)
		testParallel(t, "Equal", func(t *testing.T) {
			assert.True(t, k.Equal(k))
			assert.True(t, k.Equal(k.Key))
			assert.False(t, k.Equal(k2))
			assert.False(t, k.Equal(k2.Key))
		})
		testParallel(t, "Public", func(t *testing.T) {
			pub := k.Public().(interface {
				Equal(x crypto.PublicKey) bool
			})
			assert.True(t, pub.Equal(k.Public()))
			assert.False(t, pub.Equal(k2.Public()))
		})
		testParallel(t, "Sign", func(t *testing.T) {
			val := []byte{1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8}
			sig, err := k.Sign(rand.Reader, val, crypto.Hash(0))
			assert.NoError(t, err)
			var fn func(k *Key) bool
			switch {
			case kt&RSA == RSA:
				fn = func(k *Key) bool {
					return rsa.VerifyPKCS1v15(k.Public().(*rsa.PublicKey), crypto.Hash(0), val, sig) == nil
				}
			case kt&ECDSA == ECDSA:
				fn = func(k *Key) bool { return ecdsa.VerifyASN1(k.Public().(*ecdsa.PublicKey), val, sig) }
			case kt&ED25519 == ED25519:
				fn = func(k *Key) bool { return ed25519.Verify(k.Public().(ed25519.PublicKey), val, sig) }
			default:
				panic("shouldn't be here")
			}
			assert.True(t, fn(k))
			assert.False(t, fn(k2))
		})
	})
}

func tryDecodeKey(t *testing.T, kf KeyFormat, blk *pem.Block) (any, error) {
	var key any
	var err error
	switch kf & KeyEncodingMask {
	case PKCS1:
		assert.Equal(t, "RSA PRIVATE KEY", blk.Type)
		key, err = x509.ParsePKCS1PrivateKey(blk.Bytes)
	case PKCS8:
		assert.Equal(t, "PRIVATE KEY", blk.Type)
		key, err = x509.ParsePKCS8PrivateKey(blk.Bytes)
	case SEC1:
		assert.Equal(t, "EC PRIVATE KEY", blk.Type)
		key, err = x509.ParseECPrivateKey(blk.Bytes)
	default:
		panic("shouldn't be here")
	}
	return key, err
}

func tryDecodePublicKey(t *testing.T, kf KeyFormat, blk *pem.Block) (any, error) {
	var key any
	var err error
	switch kf & KeyEncodingMask {
	case PKCS1:
		assert.Equal(t, "RSA PUBLIC KEY", blk.Type)
		key, err = x509.ParsePKCS1PublicKey(blk.Bytes)
	case PKCS8:
		fallthrough
	case SEC1:
		assert.Equal(t, "PUBLIC KEY", blk.Type)
		key, err = x509.ParsePKIXPublicKey(blk.Bytes)
	default:
		panic("shouldn't be here")
	}
	return key, err
}

func tryDecryptKey(t *testing.T, kf KeyFormat, blk *pem.Block, pass []byte) (any, error) {
	der, err := pemutil.DecryptPEMBlock(blk, pass)
	var key any
	if err == nil {
		switch kf & KeyEncodingMask {
		case PKCS1:
			assert.Equal(t, "RSA PRIVATE KEY", blk.Type)
			key, err = x509.ParsePKCS1PrivateKey(der)
		case PKCS8:
			assert.Equal(t, "ENCRYPTED PRIVATE KEY", blk.Type)
			key, err = x509.ParsePKCS8PrivateKey(der)
		case SEC1:
			assert.Equal(t, "EC PRIVATE KEY", blk.Type)
			key, err = x509.ParseECPrivateKey(der)
		default:
			panic("shouldn't be here")
		}
	}
	return key, err
}
