package cat

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"
	"os"
)

// Represents a generated chain of certificate pairs, ordered leaf-first.
type Chain []*Pair

// Generate a certificate chain according to the provided templates. The first template is
// assumed to represent a self-signed root CA, and any subsequent templates are assumed to
// represent intermediate CAs issued in sequence from that root.
//
// If Key field in the template is populated it will be used as private key for that pair,
// otherwise a new private key is generated according to the value of the KeyFormat field.
// If neither Key or KeyFormat are populated the template will inherit the KeyFormat of its
// parent template and a new private key will be generated.
//
// The contents of the Certificate struct in each template are parsed identically to (and ultimately
// by) x509.CreateCertificate, but with some additional logic allowing them to be automatically
// populated beforehand if left unset:
//   - If the SerialNumber field is unpopulated it will either inherit the serial number
//     of the parent template (incremented by one), or be set to 1 if no parent exists.
//   - If both NotBefore and NotAfter are unpopulated they will either inherit the
//     validity period of the parent template, or be set to a 24-hour period starting from
//     the current time if no parent exists.
//   - If all of KeyUsage, ExtKeyUsage, IsCA, BasicConstraintsValid, MaxPathLen, and MaxPathLenZero are
//     unpopulated then the template will be configured as a CA certificate with unlimited path length.
//   - If MaxPathLen or MaxPathLenZero are populated but all of KeyUsage, ExtKeyUsage, IsCA, and
//     BasicConstraintsValid are not then the template will be configured as a CA certificate with the
//     specified path length.
//
// The resulting certificate chain is returned in reverse order relative to the templates, starting
// with the leaf-most certificate pair.
func GenerateChain(tmps []*Template) (Chain, error) {
	return generateChain(nil, tmps, tuChain)
}

// Alternate version of GenerateChain that panics on error.
func GenerateChainP(tmps []*Template) Chain {
	ch, err := GenerateChain(tmps)
	if err != nil {
		panic(fmt.Errorf("failed to generate chain; %w", err))
	}
	return ch
}

// Generate a certificate chain ending with a client end-entity certificate, according to the
// provided templates.
//
// This function behaves identically to GenerateChain, save for the fact that the leaf-most
// certificate in the chain will be configured as a client end-entity certificate rather than
// a CA certificate if none of its KeyUsage, ExtKeyUsage, IsCA, MaxPathLen, MaxPathLenZero,
// or BasicConstraintsValid fields are populated in the template.
//
// If the provided chain consists of a single template, a single self-signed client end-entity
// certificate pair will be produced.
func GenerateClientChain(tmps []*Template) (Chain, error) {
	end := len(tmps) - 1
	if end < 0 {
		return nil, nil
	}
	ch, err := generateChain(nil, tmps[:end], tuChain)
	if err != nil {
		return nil, err
	}
	return generateChain(ch, tmps[end:], tuClient)
}

// Alternate version of GenerateClientChain that panics on error.
func GenerateClientChainP(tmps []*Template) Chain {
	ch, err := GenerateClientChain(tmps)
	if err != nil {
		panic(fmt.Errorf("failed to generate chain; %w", err))
	}
	return ch
}

// Generate a certificate chain ending with a server end-entity certificate, according to the
// provided templates.
//
// This function behaves identically to GenerateChain, save for the fact that the leaf-most
// certificate in the chain will be configured as a server end-entity certificate rather than
// a CA certificate if none of its KeyUsage, ExtKeyUsage, IsCA, MaxPathLen, MaxPathLenZero,
// or BasicConstraintsValid fields are populated in the template.
//
// If the provided chain consists of a single template, a single self-signed server end-entity
// certificate pair will be produced.
func GenerateServerChain(tmps []*Template) (Chain, error) {
	end := len(tmps) - 1
	if end < 0 {
		return nil, nil
	}
	ch, err := generateChain(nil, tmps[:end], tuChain)
	if err != nil {
		return nil, err
	}
	return generateChain(ch, tmps[end:], tuServer)
}

// Alternate version of GenerateServerChain that panics on error.
func GenerateServerChainP(tmps []*Template) Chain {
	ch, err := GenerateServerChain(tmps)
	if err != nil {
		panic(fmt.Errorf("failed to generate chain; %w", err))
	}
	return ch
}

// Decode a chain of PEM-encoded certificate pairs.
//
// This function attempts to consume the entire input, and will return
// an error if it does not find at least one PEM-encoded certificate or
// certificate pair, or if it encounters any unrecognised PEM blocks.
// If pass is not nil it will be used as a passphrase to attempt to
// decrypt private keys, otherwise encrypted keys will be rejected.
func DecodeChain(data, pass []byte) (Chain, error) {
	var ch Chain
	for blk, rest := pem.Decode(data); blk != nil; blk, rest = pem.Decode(rest) {
		var pair *Pair
		var err error
		pair, rest, err = decodePair(blk, rest, pass)
		if err != nil {
			return nil, err
		}
		ch = append(ch, pair)
	}
	if len(ch) == 0 {
		return nil, fmt.Errorf("no certificate found")
	}
	return ch, nil
}

// Alternate version of DecodeChain that panics on error.
func DecodeChainP(data, pass []byte) Chain {
	ch, err := DecodeChain(data, pass)
	if err != nil {
		panic(fmt.Errorf("failed to decode chain; %w", err))
	}
	return ch
}

// Read and decode a chain of PEM-encoded certificate pairs from a file
//
// This function attempts to consume the entire file, and will return
// an error if it does not find at least one PEM-encoded certificate or
// certificate pair, or if it encounters any unrecognised PEM blocks.
// If pass is not nil it will be used as a passphrase to attempt to
// decrypt private keys, otherwise encrypted keys will be rejected.
func ReadChainFile(name string, pass []byte) (Chain, error) {
	data, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	return DecodeChain(data, pass)
}

// Alternate version of ReadChainFile that panics on error.
func ReadChainFileP(name string, pass []byte) Chain {
	ch, err := ReadChainFile(name, pass)
	if err != nil {
		panic(fmt.Errorf("failed to read chain; %w", err))
	}
	return ch
}

// Generate a new certificate chain according to the provided templates, using this chain as
// the issuing CA.
//
// This function behaves identically to GenerateChain, save for the fact that the leaf-most
// certificate in this chain will be used as the issuing CA for the new chain, rather than
// it beginning with a self-signed root.
//
// The resulting certificate chain will be a concatenation of the two, starting with the
// leaf-most certificate pair.
func (ch Chain) IssueChain(tmps []*Template) (Chain, error) {
	return generateChain(ch, tmps, tuChain)
}

// Alternate version of IssueChain that panics on error.
func (ch Chain) IssueChainP(tmps []*Template) Chain {
	ch, err := ch.IssueChain(tmps)
	if err != nil {
		panic(fmt.Errorf("failed to generate chain; %w", err))
	}
	return ch
}

// Generate a new certificate chain according to the provided templates, ending with a
// client end-entity certificate, and using this chain as the issuing CA.
//
// This function behaves identically to GenerateClientChain, save for the fact that the
// leaf-most certificate in this chain will be used as the issuing CA for the new chain,
// rather than it beginning with a self-signed root.
//
// The resulting certificate chain will be a concatenation of the two, starting with the
// leaf-most certificate pair.
func (ch Chain) IssueClientChain(tmps []*Template) (Chain, error) {
	end := len(tmps) - 1
	if end < 0 {
		return ch, nil
	}
	ch, err := generateChain(ch, tmps[:end], tuChain)
	if err != nil {
		return nil, err
	}
	return generateChain(ch, tmps[end:], tuClient)
}

// Alternate version of IssueClientChain that panics on error.
func (ch Chain) IssueClientChainP(tmps []*Template) Chain {
	ch, err := ch.IssueClientChain(tmps)
	if err != nil {
		panic(fmt.Errorf("failed to generate chain; %w", err))
	}
	return ch
}

// Generate a new certificate chain according to the provided templates, ending with a
// server end-entity certificate, and using this chain as the issuing CA.
//
// This function behaves identically to GenerateServerChain, save for the fact that the
// leaf-most certificate in this chain will be used as the issuing CA for the new chain,
// rather than it beginning with a self-signed root.
//
// The resulting certificate chain will be a concatenation of the two, starting with the
// leaf-most certificate pair.
func (ch Chain) IssueServerChain(tmps []*Template) (Chain, error) {
	end := len(tmps) - 1
	if end < 0 {
		return ch, nil
	}
	ch, err := generateChain(ch, tmps[:end], tuChain)
	if err != nil {
		return nil, err
	}
	return generateChain(ch, tmps[end:], tuServer)
}

// Alternate version of IssueServerChain that panics on error.
func (ch Chain) IssueServerChainP(tmps []*Template) Chain {
	ch, err := ch.IssueServerChain(tmps)
	if err != nil {
		panic(fmt.Errorf("failed to generate chain; %w", err))
	}
	return ch
}

// Encode certificate chain as PEM, optionally preceded by a plain-text copy of the leaf
// certificate private key.
func (ch Chain) Encode(withKey bool) ([]byte, error) {
	var all []byte
	for i, p := range ch {
		crt, err := p.Encode(withKey && i == 0)
		if err != nil {
			return nil, err
		}
		all = append(all, crt...)
	}
	return all, nil
}

// Alternate version of Encode that panics on error.
func (ch Chain) EncodeP(withKey bool) []byte {
	pem, err := ch.Encode(withKey)
	if err != nil {
		panic(fmt.Errorf("failed to encode pair; %w", err))
	}
	return pem
}

// Encode certificate chain as PEM, preceded by a passphrase-encrypted copy of the leaf
// certificate private key.
func (ch Chain) Encrypt(alg x509.PEMCipher, pass []byte) ([]byte, error) {
	var all []byte
	for i, p := range ch {
		var crt []byte
		var err error
		if i == 0 {
			crt, err = p.Encrypt(alg, pass)
		} else {
			crt, err = p.Encode(false)
		}
		if err != nil {
			return nil, err
		}
		all = append(all, crt...)
	}
	return all, nil
}

// Alternate version of Encrypt that panics on error.
func (ch Chain) EncryptP(alg x509.PEMCipher, pass []byte) []byte {
	pem, err := ch.Encrypt(alg, pass)
	if err != nil {
		panic(fmt.Errorf("failed to encode chain; %w", err))
	}
	return pem
}

// Encode certificate chain as PEM, optionally preceded by a plain-text copy of the leaf
// certificate private key, then write it to a stream.
func (ch Chain) Write(out io.Writer, withKey bool) error {
	for i, p := range ch {
		if err := p.Write(out, withKey && i == 0); err != nil {
			return err
		}
	}
	return nil
}

// Alternate version of Write that panics on error.
func (ch Chain) WriteP(out io.Writer, withKey bool) {
	if err := ch.Write(out, withKey); err != nil {
		panic(fmt.Errorf("failed to write chain; %w", err))
	}
}

// Encode certificate chain as PEM, preceded by a passphrase-encrypted copy of the leaf
// certificate private key, then write it to a stream.
func (ch Chain) WriteEncrypted(out io.Writer, alg x509.PEMCipher, pass []byte) error {
	for i, p := range ch {
		var err error
		if i == 0 {
			err = p.WriteEncrypted(out, alg, pass)
		} else {
			err = p.Write(out, false)
		}
		if err != nil {
			return err
		}
	}
	return nil
}

// Alternate version of WriteEncrypted that panics on error.
func (ch Chain) WriteEncryptedP(out io.Writer, alg x509.PEMCipher, pass []byte) {
	if err := ch.WriteEncrypted(out, alg, pass); err != nil {
		panic(fmt.Errorf("failed to write chain; %w", err))
	}
}

// Encode certificate chain as PEM, optionally preceded by a plain-text copy of the leaf
// certificate private key, then write it to a file.
func (ch Chain) WriteFile(name string, mode os.FileMode, withKey bool) error {
	out, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, mode)
	if err != nil {
		return err
	}
	defer out.Close()
	return ch.Write(out, withKey)
}

// Alternate version of WriteFile that panics on error.
func (ch Chain) WriteFileP(name string, mode os.FileMode, withKey bool) {
	if err := ch.WriteFile(name, mode, withKey); err != nil {
		panic(fmt.Errorf("failed to write chain; %w", err))
	}
}

// Encode certificate chain as PEM, preceded by a passphrase-encrypted copy of the leaf
// certificate private key, then write it to a file.
func (ch Chain) WriteEncryptedFile(name string, mode os.FileMode, alg x509.PEMCipher, pass []byte) error {
	out, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, mode)
	if err != nil {
		return err
	}
	defer out.Close()
	return ch.WriteEncrypted(out, alg, pass)
}

// Alternate version of WriteEncryptedFile that panics on error.
func (ch Chain) WriteEncryptedFileP(name string, mode os.FileMode, alg x509.PEMCipher, pass []byte) {
	if err := ch.WriteEncryptedFile(name, mode, alg, pass); err != nil {
		panic(fmt.Errorf("failed to write chain; %w", err))
	}
}

// Returns a slice from the front of the chain; non-negative values specify
// how many pairs to keep, negative values specify how many pairs to drop.
func (ch Chain) Head(num int) Chain {
	if num < 0 {
		return ch[:len(ch)+num]
	} else {
		return ch[:num]
	}
}

// Returns a slice from the back of the chain; non-negative values specify
// how many pairs to keep, negative values specify how many pairs to drop.
func (ch Chain) Tail(num int) Chain {
	if num < 0 {
		return ch[-num:]
	} else {
		return ch[len(ch)-num:]
	}
}

// Returns this chain as an x509.Certificate array.
func (ch Chain) X509() []*x509.Certificate {
	var crts []*x509.Certificate
	for _, p := range ch {
		crts = append(crts, p.Cert)
	}
	return crts
}

// Return a tls.Certificate instance created from this chain.
func (ch Chain) TLS() *tls.Certificate {
	if len(ch) == 0 {
		panic(fmt.Errorf("empty chain"))
	}
	var crts [][]byte
	for _, p := range ch {
		crts = append(crts, p.Cert.Raw)
	}
	return &tls.Certificate{
		Leaf:        ch[0].Cert,
		PrivateKey:  ch[0].Key,
		Certificate: crts,
	}
}

// Return an x509.CertPool instance containing the root-most certificate in this chain.
func (ch Chain) CertPool() *x509.CertPool {
	if len(ch) == 0 {
		panic(fmt.Errorf("empty chain"))
	}
	return ch[len(ch)-1].CertPool()
}

// Returns true if the root-most certificate in this chain is self-signed.
func (ch Chain) SelfSigned() bool {
	if len(ch) == 0 {
		panic(fmt.Errorf("empty chain"))
	}
	return ch[len(ch)-1].SelfSigned()
}

// Returns a string representation of this certificate chain.
func (ch Chain) String() string {
	s := "Chain{"
	for i, p := range ch {
		s = fmt.Sprintf("%s\n[%d]=%s,", s, i, p)
	}
	return s + "}"
}
