package cat

import (
	"crypto/rand"
	"crypto/x509"
	"math/big"
	"time"
)

// Represents an X.509 certificate pair to be generated.
type Template struct {
	x509.Certificate

	Key       *Key      // Private key for this certificate pair.
	KeyFormat KeyFormat // Private key format to generate for this certificate pair if Key is nil.
}

var tuChain = &x509.Certificate{
	IsCA:                  true,
	BasicConstraintsValid: true,
	MaxPathLen:            -1,
	KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageCRLSign,
}

var tuClient = &x509.Certificate{
	IsCA:                  false,
	BasicConstraintsValid: true,
	MaxPathLen:            -1,
	KeyUsage:              x509.KeyUsageDigitalSignature,
	ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth},
}

var tuServer = &x509.Certificate{
	IsCA:                  false,
	BasicConstraintsValid: true,
	MaxPathLen:            -1,
	KeyUsage:              x509.KeyUsageDigitalSignature,
	ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
}

func generateChain(ca Chain, tmps []*Template, tu *x509.Certificate) (Chain, error) {
	var ch Chain
	if len(ca) > 0 {
		ch = make(Chain, len(ca))
		copy(ch, ca)
	}
	for _, ts := range tmps {
		var err error
		if len(ch) > 0 {
			ts, err = prepareTemplate(ts, tu, ch[0].Cert, ch[0].Key)
		} else {
			ts, err = prepareTemplate(ts, tu, nil, nil)
		}
		if err != nil {
			return nil, err
		}
		var der []byte
		if len(ch) > 0 {
			der, err = x509.CreateCertificate(rand.Reader, &ts.Certificate, ch[0].Cert, ts.Key.Public(), ch[0].Key)
		} else {
			der, err = x509.CreateCertificate(rand.Reader, &ts.Certificate, &ts.Certificate, ts.Key.Public(), ts.Key)
		}
		if err != nil {
			return nil, err
		}
		crt, err := x509.ParseCertificate(der)
		if err != nil {
			return nil, err
		}
		ch = append(Chain{{Key: ts.Key, Cert: crt}}, ch...)
	}
	return ch, nil
}

// Return a copy of the subject template with unset fields populated
// from a combination of the issuer template and a default set of key
// usage and basic contraints, and generate a private key if none is
// provided
func prepareTemplate(ts *Template, tu, ti *x509.Certificate, ki *Key) (*Template, error) {
	t := *ts
	if t.Key == nil {
		if t.KeyFormat == 0 && ki != nil {
			t.KeyFormat = ki.Format
		}
		key, err := GenerateKey(t.KeyFormat)
		if err != nil {
			return nil, err
		}
		t.Key = key
	}
	if t.SerialNumber == nil {
		sn := big.NewInt(1)
		if ti == nil {
			t.SerialNumber = sn
		} else {
			t.SerialNumber = sn.Add(sn, ti.SerialNumber)
		}
	}
	if templateTimeZero(&t) {
		if ti == nil {
			t.NotBefore = time.Now()
			t.NotAfter = t.NotBefore.Add(time.Hour * 24)
		} else {
			t.NotBefore = ti.NotBefore
			t.NotAfter = ti.NotAfter
		}
	}
	if tu != nil && templateUsageZero(&t, tu.IsCA) {
		t.KeyUsage = tu.KeyUsage
		t.ExtKeyUsage = tu.ExtKeyUsage
		t.IsCA = tu.IsCA
		t.BasicConstraintsValid = tu.BasicConstraintsValid
		if t.MaxPathLen == 0 && !t.MaxPathLenZero {
			t.MaxPathLen = tu.MaxPathLen
			t.MaxPathLenZero = tu.MaxPathLenZero
		}
	}
	return &t, nil
}

// Return true if template validity period is entirely unset.
func templateTimeZero(t *Template) bool {
	return t.NotBefore.IsZero() && t.NotAfter.IsZero()
}

// Return true if key usage and basic constraints are entirely unset.
func templateUsageZero(t *Template, skipPathLen bool) bool {
	return t.KeyUsage == 0 &&
		t.ExtKeyUsage == nil &&
		!t.IsCA &&
		!t.BasicConstraintsValid &&
		(skipPathLen ||
			(t.MaxPathLen == 0 && !t.MaxPathLenZero))
}
