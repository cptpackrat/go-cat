package cat

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"

	"go.step.sm/crypto/pemutil"
)

// Represents a combination of a private key algorithm, generation parameters
// such as key size or elliptic curve, and marshaling/encoding parameters.
type KeyFormat int

const (
	// Portion of a KeyFormat that describes private key algorithm and parameters.
	KeyTypeMask KeyFormat = 0x00ffff
	// Portion of a KeyFormat that describes private key encoding format.
	KeyEncodingMask KeyFormat = 0xff0000
)

const (
	// RSA key.
	RSA KeyFormat = 0x000001 << iota
	// ECDSA key.
	ECDSA
	// Ed25519 key.
	ED25519
)

const (
	// 1024-bit RSA key.
	RSA_1024 KeyFormat = RSA | 0x000100<<iota
	// 2048-bit RSA key.
	RSA_2048
	// 3072-bit RSA key.
	RSA_3072
	// 4096-bit RSA key.
	RSA_4096
)

const (
	// ECDSA key using NIST P-224.
	ECDSA_P224 KeyFormat = ECDSA | 0x000100<<iota
	// ECDSA key using NIST P-256.
	ECDSA_P256
	// ECDSA key using NIST P-384.
	ECDSA_P384
	// ECDSA key using NIST P-521.
	ECDSA_P521
)

const (
	// Marshal with PKCS #1.
	PKCS1 KeyFormat = 0x010000 << iota
	// Marshal with PKCS #8.
	PKCS8
	// Marshal with SEC 1.
	SEC1
)

// Private key type and encoding combinations.
const (
	// 1024-bit RSA key marshaled using PKCS #1.
	RSA_1024_PKCS1 KeyFormat = RSA_1024 | PKCS1
	// 2048-bit RSA key marshaled using PKCS #1.
	RSA_2048_PKCS1 KeyFormat = RSA_2048 | PKCS1
	// 3072-bit RSA key marshaled using PKCS #1.
	RSA_3072_PKCS1 KeyFormat = RSA_3072 | PKCS1
	// 4096-bit RSA key marshaled using PKCS #1.
	RSA_4096_PKCS1 KeyFormat = RSA_4096 | PKCS1
	// 1024-bit RSA key marshaled using PKCS #8.
	RSA_1024_PKCS8 KeyFormat = RSA_1024 | PKCS8
	// 2048-bit RSA key marshaled using PKCS #8.
	RSA_2048_PKCS8 KeyFormat = RSA_2048 | PKCS8
	// 3072-bit RSA key marshaled using PKCS #8.
	RSA_3072_PKCS8 KeyFormat = RSA_3072 | PKCS8
	// 4096-bit RSA key marshaled using PKCS #8.
	RSA_4096_PKCS8 KeyFormat = RSA_4096 | PKCS8
	// ECDSA key using NIST P-224 marshaled using SEC 1.
	ECDSA_P224_SEC1 KeyFormat = ECDSA_P224 | SEC1
	// ECDSA key using NIST P-256 marshaled using SEC 1.
	ECDSA_P256_SEC1 KeyFormat = ECDSA_P256 | SEC1
	// ECDSA key using NIST P-384 marshaled using SEC 1.
	ECDSA_P384_SEC1 KeyFormat = ECDSA_P384 | SEC1
	// ECDSA key using NIST P-521 marshaled using SEC 1.
	ECDSA_P521_SEC1 KeyFormat = ECDSA_P521 | SEC1
	// ECDSA key using NIST P-224 marshaled using PKCS #8.
	ECDSA_P224_PKCS8 KeyFormat = ECDSA_P224 | PKCS8
	// ECDSA key using NIST P-256 marshaled using PKCS #8.
	ECDSA_P256_PKCS8 KeyFormat = ECDSA_P256 | PKCS8
	// ECDSA key using NIST P-384 marshaled using PKCS #8.
	ECDSA_P384_PKCS8 KeyFormat = ECDSA_P384 | PKCS8
	// ECDSA key using NIST P-521 marshaled using PKCS #8.
	ECDSA_P521_PKCS8 KeyFormat = ECDSA_P521 | PKCS8
	// Ed25519 key marshaled using PKCS #8.
	ED25519_PKCS8 KeyFormat = ED25519 | PKCS8
)

func (kf KeyFormat) generate() (any, error) {
	switch kf & KeyTypeMask {
	case RSA_1024:
		return rsa.GenerateKey(rand.Reader, 1024)
	case RSA_2048:
		return rsa.GenerateKey(rand.Reader, 2048)
	case RSA_3072:
		return rsa.GenerateKey(rand.Reader, 3072)
	case RSA_4096:
		return rsa.GenerateKey(rand.Reader, 4096)
	case ECDSA_P224:
		return ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	case ECDSA_P256:
		return ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	case ECDSA_P384:
		return ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	case ECDSA_P521:
		return ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	case ED25519:
		_, key, err := ed25519.GenerateKey(rand.Reader)
		return key, err
	}
	return nil, fmt.Errorf("cannot generate unknown key format")
}

func (kf KeyFormat) marshal(key any) ([]byte, error) {
	switch kf & KeyEncodingMask {
	case PKCS1:
		if k, ok := key.(*rsa.PrivateKey); ok {
			return x509.MarshalPKCS1PrivateKey(k), nil
		}
		return nil, fmt.Errorf("unsupported key type for PKCS1")
	case PKCS8:
		return x509.MarshalPKCS8PrivateKey(key)
	case SEC1:
		if k, ok := key.(*ecdsa.PrivateKey); ok {
			return x509.MarshalECPrivateKey(k)
		}
		return nil, fmt.Errorf("unsupported key type for SEC1")
	}
	return nil, fmt.Errorf("cannot marshal unknown key format")
}

func (kf KeyFormat) marshalPublic(key any) ([]byte, error) {
	switch kf & KeyEncodingMask {
	case PKCS1:
		if k, ok := key.(*rsa.PrivateKey); ok {
			return x509.MarshalPKCS1PublicKey(&k.PublicKey), nil
		}
		return nil, fmt.Errorf("unsupported key type for PKCS1")
	case PKCS8:
		if k, ok := key.(*rsa.PrivateKey); ok {
			return x509.MarshalPKIXPublicKey(&k.PublicKey)
		}
		if k, ok := key.(*ecdsa.PrivateKey); ok {
			return x509.MarshalPKIXPublicKey(&k.PublicKey)
		}
		if k, ok := key.(ed25519.PrivateKey); ok {
			return x509.MarshalPKIXPublicKey(k.Public())
		}
		return nil, fmt.Errorf("unsupported key type for PKCS8")
	case SEC1:
		if k, ok := key.(*ecdsa.PrivateKey); ok {
			return x509.MarshalPKIXPublicKey(&k.PublicKey)
		}
		return nil, fmt.Errorf("unsupported key type for SEC1")
	}
	return nil, fmt.Errorf("cannot marshal unknown key format")
}

func (kf KeyFormat) encode(key any) (*pem.Block, error) {
	der, err := kf.marshal(key)
	if err != nil {
		return nil, err
	}
	switch kf & KeyEncodingMask {
	case PKCS1:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: der}, nil
	case PKCS8:
		return &pem.Block{Type: "PRIVATE KEY", Bytes: der}, nil
	case SEC1:
		return &pem.Block{Type: "EC PRIVATE KEY", Bytes: der}, nil
	}
	return nil, fmt.Errorf("cannot encode unknown key format")
}

func (kf KeyFormat) encodePublic(key any) (*pem.Block, error) {
	der, err := kf.marshalPublic(key)
	if err != nil {
		return nil, err
	}
	switch kf & KeyEncodingMask {
	case PKCS1:
		return &pem.Block{Type: "RSA PUBLIC KEY", Bytes: der}, nil
	case PKCS8:
		fallthrough
	case SEC1:
		return &pem.Block{Type: "PUBLIC KEY", Bytes: der}, nil
	}
	return nil, fmt.Errorf("cannot encode unknown key format")
}

func (kf KeyFormat) encrypt(key any, alg x509.PEMCipher, pass []byte) (*pem.Block, error) {
	der, err := kf.marshal(key)
	if err != nil {
		return nil, err
	}
	switch kf & KeyEncodingMask {
	case PKCS1:
		//lint:ignore SA1019 legacy formats needed for testing
		return x509.EncryptPEMBlock(rand.Reader, "RSA PRIVATE KEY", der, pass, alg)
	case PKCS8:
		return pemutil.EncryptPKCS8PrivateKey(rand.Reader, der, pass, alg)
	case SEC1:
		//lint:ignore SA1019 legacy formats needed for testing
		return x509.EncryptPEMBlock(rand.Reader, "EC PRIVATE KEY", der, pass, alg)
	}
	return nil, fmt.Errorf("cannot encrypt unknown key format")
}

func (kf KeyFormat) String() string {
	name := "UNKNOWN"
	if kf&KeyTypeMask == 0 {
		switch kf & KeyEncodingMask {
		case PKCS1:
			name = "PKCS1"
		case PKCS8:
			name = "PKCS8"
		case SEC1:
			name = "SEC1"
		}
	} else {
		switch kf & KeyTypeMask {
		case RSA_1024:
			name = "RSA_1024"
		case RSA_2048:
			name = "RSA_2048"
		case RSA_3072:
			name = "RSA_3072"
		case RSA_4096:
			name = "RSA_4096"
		case RSA:
			name = "RSA"
		case ECDSA_P224:
			name = "ECDSA_P224"
		case ECDSA_P256:
			name = "ECDSA_P256"
		case ECDSA_P384:
			name = "ECDSA_P384"
		case ECDSA_P521:
			name = "ECDSA_P521"
		case ECDSA:
			name = "ECDSA"
		case ED25519:
			name = "ED25519"
		}
		switch kf & KeyEncodingMask {
		case PKCS1:
			name += "_PKCS1"
		case PKCS8:
			name += "_PKCS8"
		case SEC1:
			name += "_SEC1"
		}
	}
	return name
}

// Determine the key format for the given private key.
func IdentifyKey(key any) (KeyFormat, error) {
	if k, ok := key.(*rsa.PrivateKey); ok && k != nil {
		switch k.Size() {
		case 128:
			return RSA_1024, nil
		case 256:
			return RSA_2048, nil
		case 384:
			return RSA_3072, nil
		case 512:
			return RSA_4096, nil
		}
		return RSA, nil
	} else if k, ok := key.(*ecdsa.PrivateKey); ok && k != nil {
		switch k.Curve {
		case elliptic.P224():
			return ECDSA_P224, nil
		case elliptic.P256():
			return ECDSA_P256, nil
		case elliptic.P384():
			return ECDSA_P384, nil
		case elliptic.P521():
			return ECDSA_P521, nil
		}
		return ECDSA, nil
	} else if k, ok := key.(ed25519.PrivateKey); ok && k != nil {
		return ED25519, nil
	}
	return 0, fmt.Errorf("cannot identify unknown key format")
}

// Alternate version of IdentifyKey that panics on error.
func IdentifyKeyP(key any) KeyFormat {
	kf, err := IdentifyKey(key)
	if err != nil {
		panic(fmt.Errorf("failed to identify key; %w", err))
	}
	return kf
}
