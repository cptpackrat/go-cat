module gitlab.com/cptpackrat/go-cat

go 1.22

require (
	github.com/stretchr/testify v1.10.0
	go.step.sm/crypto v0.55.0
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	go.uber.org/mock v0.5.0 // indirect
	golang.org/x/crypto v0.31.0 // indirect
	golang.org/x/mod v0.18.0 // indirect
	golang.org/x/sync v0.9.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/tools v0.22.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
