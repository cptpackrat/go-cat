// Package cat implements a set of shorthand/utility functions for performing CA tasks during testing.
package cat

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"math/big"
	"time"
)

// Alternate version of time.Parse that panics on error.
func ParseTimeP(layout, value string) time.Time {
	t, err := time.Parse(layout, value)
	if err != nil {
		panic(fmt.Errorf("failed to parse timestamp; %w", err))
	}
	return t
}

// Alternate version of time.ParseDuration that panics on error.
func ParseDurationP(value string) time.Duration {
	d, err := time.ParseDuration(value)
	if err != nil {
		panic(fmt.Errorf("failed to parse duration; %w", err))
	}
	return d
}

// Return a slice of len random bytes from rand.Read.
func RandBytes(len int) ([]byte, error) {
	dst := make([]byte, len)
	if _, err := rand.Read(dst); err != nil {
		return nil, err
	}
	return dst, nil
}

// Alternate version of RandBytes that panics on error.
func RandBytesP(len int) []byte {
	b, err := RandBytes(len)
	if err != nil {
		panic(fmt.Errorf("failed to get random bytes; %w", err))
	}
	return b
}

// Return a slice of len hex-encoded random bytes from rand.Read.
func RandHexBytes(len int) ([]byte, error) {
	src := make([]byte, len)
	dst := make([]byte, len*2)
	if _, err := rand.Read(src); err != nil {
		return nil, err
	}
	hex.Encode(dst, src)
	return dst, nil
}

// Alternate version of RandHexBytes that panics on error.
func RandHexBytesP(len int) []byte {
	b, err := RandHexBytes(len)
	if err != nil {
		panic(fmt.Errorf("failed to get random bytes; %w", err))
	}
	return b
}

// Return a random big.Int from rand.Int in [0, 1 << bitMax).
func RandInt(bitMax uint) (*big.Int, error) {
	var m big.Int
	return rand.Int(rand.Reader, m.Lsh(big.NewInt(1), bitMax))
}

// Alternate version of RandInt that panics on error.
func RandIntP(bitMax uint) *big.Int {
	n, err := RandInt(bitMax)
	if err != nil {
		panic(fmt.Errorf("failed to get random number; %w", err))
	}
	return n
}
