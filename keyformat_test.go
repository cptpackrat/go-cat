package cat

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rsa"
	"crypto/x509"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.step.sm/crypto/pemutil"
)

type rsaCase struct {
	kf   KeyFormat
	size int
	err  string
}

type ecdsaCase struct {
	kf    KeyFormat
	curve elliptic.Curve
	err   string
}

type stringCase struct {
	kf   KeyFormat
	name string
}

func (c rsaCase) String() string    { return c.kf.String() }
func (c ecdsaCase) String() string  { return c.kf.String() }
func (c stringCase) String() string { return c.name }

func TestKeyFormat(t *testing.T) {
	t.Parallel()
	testParallel(t, "generate", func(t *testing.T) {
		testParallelForEach(t, []rsaCase{
			{RSA, 0, "unknown key format"},
			{RSA_1024, 128, ""},
			{RSA_2048, 256, ""},
			{RSA_3072, 384, ""},
			{RSA_4096, 512, ""},
		}, func(t *testing.T, c rsaCase) {
			k1, err := c.kf.generate()
			if c.err != "" {
				assert.ErrorContains(t, err, c.err)
			} else {
				assert.NoError(t, err)
				k2, ok := k1.(*rsa.PrivateKey)
				assert.True(t, ok)
				assert.Equal(t, c.size, k2.Size())
			}
		})
		testParallelForEach(t, []ecdsaCase{
			{ECDSA, nil, "unknown key format"},
			{ECDSA_P224, elliptic.P224(), ""},
			{ECDSA_P256, elliptic.P256(), ""},
			{ECDSA_P384, elliptic.P384(), ""},
			{ECDSA_P521, elliptic.P521(), ""},
		}, func(t *testing.T, c ecdsaCase) {
			k1, err := c.kf.generate()
			if c.err != "" {
				assert.ErrorContains(t, err, c.err)
			} else {
				assert.NoError(t, err)
				k2, ok := k1.(*ecdsa.PrivateKey)
				assert.True(t, ok)
				assert.Equal(t, c.curve, k2.Curve)
			}
		})
		testParallel(t, "ED25519", func(t *testing.T) {
			k, err := ED25519.generate()
			assert.NoError(t, err)
			_, ok := k.(ed25519.PrivateKey)
			assert.True(t, ok)
		})
	})
	testParallel(t, "encode", func(t *testing.T) {
		testParallelForEach(t, []KeyFormat{
			RSA_1024,
			RSA_2048,
			RSA_3072,
			RSA_4096,
			ECDSA_P224,
			ECDSA_P256,
			ECDSA_P384,
			ECDSA_P521,
			ED25519,
		}, func(t *testing.T, kf KeyFormat) {
			k1, err := kf.generate()
			assert.NoError(t, err)
			testParallel(t, "PKCS1", func(t *testing.T) {
				blk, err := (kf | PKCS1).encode(k1)
				if kf&RSA == 0 {
					assert.ErrorContains(t, err, "unsupported key type")
				} else {
					assert.NoError(t, err)
					assert.Equal(t, "RSA PRIVATE KEY", blk.Type)
					k2, err := x509.ParsePKCS1PrivateKey(blk.Bytes)
					assert.NoError(t, err)
					assert.True(t, k2.Equal(k1))
				}
			})
			testParallel(t, "PKCS8", func(t *testing.T) {
				blk, err := (kf | PKCS8).encode(k1)
				assert.NoError(t, err)
				assert.Equal(t, "PRIVATE KEY", blk.Type)
				k2, err := x509.ParsePKCS8PrivateKey(blk.Bytes)
				assert.NoError(t, err)
				assert.True(t, k2.(interface {
					Equal(x crypto.PrivateKey) bool
				}).Equal(k1))
			})
			testParallel(t, "SEC1", func(t *testing.T) {
				blk, err := (kf | SEC1).encode(k1)
				if kf&ECDSA == 0 {
					assert.ErrorContains(t, err, "unsupported key type")
				} else {
					assert.NoError(t, err)
					assert.Equal(t, "EC PRIVATE KEY", blk.Type)
					k2, err := x509.ParseECPrivateKey(blk.Bytes)
					assert.NoError(t, err)
					assert.True(t, k2.Equal(k1))
				}
			})
			testParallel(t, "UNKNOWN", func(t *testing.T) {
				_, err := kf.encode(k1)
				assert.ErrorContains(t, err, "unknown key format")
			})
		})
	})
	testParallel(t, "encodePublic", func(t *testing.T) {
		testParallelForEach(t, []KeyFormat{
			RSA_1024,
			RSA_2048,
			RSA_3072,
			RSA_4096,
			ECDSA_P224,
			ECDSA_P256,
			ECDSA_P384,
			ECDSA_P521,
			ED25519,
		}, func(t *testing.T, kf KeyFormat) {
			k1, err := kf.generate()
			p1 := k1.(interface {
				Public() crypto.PublicKey
			}).Public()
			assert.NoError(t, err)
			testParallel(t, "PKCS1", func(t *testing.T) {
				blk, err := (kf | PKCS1).encodePublic(k1)
				if kf&RSA == 0 {
					assert.ErrorContains(t, err, "unsupported key type")
				} else {
					assert.NoError(t, err)
					assert.Equal(t, "RSA PUBLIC KEY", blk.Type)
					p2, err := x509.ParsePKCS1PublicKey(blk.Bytes)
					assert.NoError(t, err)
					assert.True(t, p2.Equal(p1))
				}
			})
			testParallel(t, "PKCS8", func(t *testing.T) {
				blk, err := (kf | PKCS8).encodePublic(k1)
				assert.NoError(t, err)
				assert.Equal(t, "PUBLIC KEY", blk.Type)
				p2, err := x509.ParsePKIXPublicKey(blk.Bytes)
				assert.NoError(t, err)
				assert.True(t, p2.(interface {
					Equal(x crypto.PublicKey) bool
				}).Equal(p1))
			})
			testParallel(t, "SEC1", func(t *testing.T) {
				blk, err := (kf | SEC1).encodePublic(k1)
				if kf&ECDSA == 0 {
					assert.ErrorContains(t, err, "unsupported key type")
				} else {
					assert.NoError(t, err)
					assert.Equal(t, "PUBLIC KEY", blk.Type)
					p2, err := x509.ParsePKIXPublicKey(blk.Bytes)
					assert.NoError(t, err)
					assert.True(t, p2.(interface {
						Equal(x crypto.PublicKey) bool
					}).Equal(p1))
				}
			})
			testParallel(t, "UNKNOWN", func(t *testing.T) {
				_, err := kf.encodePublic(k1)
				assert.ErrorContains(t, err, "unknown key format")
			})
		})
	})
	testParallel(t, "encrypt", func(t *testing.T) {
		testParallelForEach(t, []KeyFormat{
			RSA_1024,
			RSA_2048,
			RSA_3072,
			RSA_4096,
			ECDSA_P224,
			ECDSA_P256,
			ECDSA_P384,
			ECDSA_P521,
			ED25519,
		}, func(t *testing.T, kf KeyFormat) {
			k1, err := kf.generate()
			assert.NoError(t, err)
			testParallel(t, "PKCS1", func(t *testing.T) {
				blk, err := (kf | PKCS1).encrypt(k1, x509.PEMCipherAES256, []byte("password"))
				if kf&RSA == 0 {
					assert.ErrorContains(t, err, "unsupported key type")
				} else {
					assert.NoError(t, err)
					assert.Equal(t, "RSA PRIVATE KEY", blk.Type)
					//lint:ignore SA1019 legacy formats needed for testing
					der, err := x509.DecryptPEMBlock(blk, []byte("password"))
					assert.NoError(t, err)
					k2, err := x509.ParsePKCS1PrivateKey(der)
					assert.NoError(t, err)
					assert.True(t, k2.Equal(k1))
				}
			})
			testParallel(t, "PKCS8", func(t *testing.T) {
				blk, err := (kf | PKCS8).encrypt(k1, x509.PEMCipherAES256, []byte("password"))
				assert.NoError(t, err)
				assert.Equal(t, "ENCRYPTED PRIVATE KEY", blk.Type)
				der, err := pemutil.DecryptPKCS8PrivateKey(blk.Bytes, []byte("password"))
				assert.NoError(t, err)
				k2, err := x509.ParsePKCS8PrivateKey(der)
				assert.NoError(t, err)
				assert.True(t, k2.(interface {
					Equal(x crypto.PrivateKey) bool
				}).Equal(k1))
			})
			testParallel(t, "SEC1", func(t *testing.T) {
				blk, err := (kf | SEC1).encrypt(k1, x509.PEMCipherAES256, []byte("password"))
				if kf&ECDSA == 0 {
					assert.ErrorContains(t, err, "unsupported key type")
				} else {
					assert.NoError(t, err)
					assert.Equal(t, "EC PRIVATE KEY", blk.Type)
					//lint:ignore SA1019 legacy formats needed for testing
					der, err := x509.DecryptPEMBlock(blk, []byte("password"))
					assert.NoError(t, err)
					k2, err := x509.ParseECPrivateKey(der)
					assert.NoError(t, err)
					assert.True(t, k2.Equal(k1))
				}
			})
			testParallel(t, "UNKNOWN", func(t *testing.T) {
				_, err := kf.encrypt(k1, x509.PEMCipherAES256, []byte("password"))
				assert.ErrorContains(t, err, "unknown key format")
			})
		})
	})
	testParallel(t, "String", func(t *testing.T) {
		testParallelForEach(t, []stringCase{
			{0, "UNKNOWN"},
			{RSA, "RSA"},
			{RSA_1024, "RSA_1024"},
			{RSA_2048, "RSA_2048"},
			{RSA_3072, "RSA_3072"},
			{RSA_4096, "RSA_4096"},
			{RSA_1024_PKCS1, "RSA_1024_PKCS1"},
			{RSA_2048_PKCS1, "RSA_2048_PKCS1"},
			{RSA_3072_PKCS1, "RSA_3072_PKCS1"},
			{RSA_4096_PKCS1, "RSA_4096_PKCS1"},
			{RSA_1024_PKCS8, "RSA_1024_PKCS8"},
			{RSA_2048_PKCS8, "RSA_2048_PKCS8"},
			{RSA_3072_PKCS8, "RSA_3072_PKCS8"},
			{RSA_4096_PKCS8, "RSA_4096_PKCS8"},
			{ECDSA, "ECDSA"},
			{ECDSA_P224, "ECDSA_P224"},
			{ECDSA_P256, "ECDSA_P256"},
			{ECDSA_P384, "ECDSA_P384"},
			{ECDSA_P521, "ECDSA_P521"},
			{ECDSA_P224_PKCS8, "ECDSA_P224_PKCS8"},
			{ECDSA_P256_PKCS8, "ECDSA_P256_PKCS8"},
			{ECDSA_P384_PKCS8, "ECDSA_P384_PKCS8"},
			{ECDSA_P521_PKCS8, "ECDSA_P521_PKCS8"},
			{ECDSA_P224_SEC1, "ECDSA_P224_SEC1"},
			{ECDSA_P256_SEC1, "ECDSA_P256_SEC1"},
			{ECDSA_P384_SEC1, "ECDSA_P384_SEC1"},
			{ECDSA_P521_SEC1, "ECDSA_P521_SEC1"},
			{ED25519, "ED25519"},
			{ED25519_PKCS8, "ED25519_PKCS8"},
			{PKCS1, "PKCS1"},
			{PKCS8, "PKCS8"},
			{SEC1, "SEC1"},
		}, func(t *testing.T, c stringCase) {
			assert.Equal(t, c.name, c.kf.String())
		})
	})
}

func TestIdentifyKey(t *testing.T) {
	t.Parallel()
	testParallelForEach(t, []KeyFormat{
		RSA_1024,
		RSA_2048,
		RSA_3072,
		RSA_4096,
		ECDSA_P224,
		ECDSA_P256,
		ECDSA_P384,
		ECDSA_P521,
		ED25519,
	}, func(t *testing.T, kf1 KeyFormat) {
		key, err := kf1.generate()
		assert.NoError(t, err)
		kf2, err := IdentifyKey(key)
		assert.NoError(t, err)
		assert.Equal(t, kf1, kf2)
	})
	testParallel(t, "UNKNOWN", func(t *testing.T) {
		_, err := IdentifyKey(nil)
		assert.ErrorContains(t, err, "unknown key format")
	})
}

func TestIdentifyKeyP(t *testing.T) {
	t.Parallel()
	key, err := ECDSA_P384.generate()
	assert.NoError(t, err)
	assert.Equal(t, ECDSA_P384, IdentifyKeyP(key))
	assert.Panics(t, func() {
		IdentifyKeyP(nil)
	})
}

func testParallel(t *testing.T, name string, fn func(t *testing.T)) {
	t.Run(name, func(t *testing.T) {
		t.Parallel()
		fn(t)
	})
}

func testParallelForEach[T interface{ String() string }](t *testing.T, arr []T, fn func(t *testing.T, el T)) {
	for _, el := range arr {
		e := el
		t.Run(e.String(), func(t *testing.T) {
			t.Parallel()
			fn(t, e)
		})
	}
}
