package cat

import (
	"crypto"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"
	"os"

	"go.step.sm/crypto/pemutil"
)

// Represents a combination of a private key and associated format information.
//
// Implements crypto.PrivateKey and crypto.Signer so it can be used
// directly with most functions expecting a standard Go private key.
type Key struct {
	crypto.PrivateKey
	crypto.Signer

	Key    any       // The actual private key object wrapped by this one.
	Format KeyFormat // The associated format for this private key.
}

// Generate a new private key according to the specified format.
func GenerateKey(kf KeyFormat) (*Key, error) {
	key, err := kf.generate()
	if err == nil {
		return &Key{
			Key:    key,
			Format: kf,
		}, nil
	}
	return nil, err
}

// Alternate version of GenerateKey that panics on error.
func GenerateKeyP(kf KeyFormat) *Key {
	key, err := GenerateKey(kf)
	if err != nil {
		panic(fmt.Errorf("failed to generate key; %w", err))
	}
	return key
}

// Decode a PEM-encoded private key and return it alongside any unconsumed bytes.
//
// Returns an error if the provided data contains no PEM blocks, or
// if the first PEM block found cannot be decoded as a private key.
// If pass is not nil it will be used as a passphrase to attempt to
// decrypt private keys, otherwise encrypted keys will be rejected.
func DecodeKey(data, pass []byte) (*Key, []byte, error) {
	blk, rest := pem.Decode(data)
	if blk == nil {
		return nil, nil, fmt.Errorf("no key found")
	}
	key, err := decodeKey(blk, pass)
	return key, rest, err
}

// Alternate version of DecodeKey that panics on error.
func DecodeKeyP(data, pass []byte) (*Key, []byte) {
	key, rest, err := DecodeKey(data, pass)
	if err != nil {
		panic(fmt.Errorf("failed to decode key; %w", err))
	}
	return key, rest
}

// Read and decode a PEM-encoded private key from a file.
//
// Returns an error if the provided file contains no PEM blocks, or
// if the first PEM block found cannot be decoded as a private key.
// If pass is not nil it will be used as a passphrase to attempt to
// decrypt private keys, otherwise encrypted keys will be rejected.
func ReadKeyFile(name string, pass []byte) (*Key, error) {
	data, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	key, _, err := DecodeKey(data, pass)
	return key, err
}

// Alternate version of ReadKeyFile that panics on error.
func ReadKeyFileP(name string, pass []byte) *Key {
	key, err := ReadKeyFile(name, pass)
	if err != nil {
		panic(fmt.Errorf("failed to read key; %w", err))
	}
	return key
}

// Encode private key as PEM according to its associated key format.
func (k *Key) Encode() ([]byte, error) {
	blk, err := k.Format.encode(k.Key)
	if err != nil {
		return nil, err
	}
	return pem.EncodeToMemory(blk), nil
}

// Alternate version of Encode that panics on error.
func (k *Key) EncodeP() []byte {
	pem, err := k.Encode()
	if err != nil {
		panic(fmt.Errorf("failed to encode key; %w", err))
	}
	return pem
}

// Encode public portion of key as PEM according to its associated key format.
//
// Most private key formats defined in this library do not have a directly
// equivalent public key format. RSA+PKCS1 formats will encode public keys
// using PKCS1; all other formats use PKIX, a.k.a SubjectPublicKeyInfo.
func (k *Key) EncodePublic() ([]byte, error) {
	blk, err := k.Format.encodePublic(k.Key)
	if err != nil {
		return nil, err
	}
	return pem.EncodeToMemory(blk), nil
}

// Alternate version of EncodePublic that panics on error.
func (k *Key) EncodePublicP() []byte {
	pem, err := k.EncodePublic()
	if err != nil {
		panic(fmt.Errorf("failed to encode public key; %w", err))
	}
	return pem
}

// Encode private key as passphrase-encrypted PEM according to its associated key format.
func (k *Key) Encrypt(alg x509.PEMCipher, pass []byte) ([]byte, error) {
	blk, err := k.Format.encrypt(k.Key, alg, pass)
	if err != nil {
		return nil, err
	}
	return pem.EncodeToMemory(blk), nil
}

// Alternate version of Encrypt that panics on error.
func (k *Key) EncryptP(alg x509.PEMCipher, pass []byte) []byte {
	pem, err := k.Encrypt(alg, pass)
	if err != nil {
		panic(fmt.Errorf("failed to encode key; %w", err))
	}
	return pem
}

// Encode private key as PEM according to its associated key format, then write it to a stream.
func (k *Key) Write(out io.Writer) error {
	blk, err := k.Format.encode(k.Key)
	if err != nil {
		return err
	}
	return pem.Encode(out, blk)
}

// Alternate version of Write that panics on error.
func (k *Key) WriteP(out io.Writer) {
	if err := k.Write(out); err != nil {
		panic(fmt.Errorf("failed to write key; %w", err))
	}
}

// Encode private key as passphrase-encrypted PEM according to its associated key format, then write it to a stream.
func (k *Key) WriteEncrypted(out io.Writer, alg x509.PEMCipher, pass []byte) error {
	blk, err := k.Format.encrypt(k.Key, alg, pass)
	if err != nil {
		return err
	}
	return pem.Encode(out, blk)
}

// Alternate version of WriteEncrypted that panics on error.
func (k *Key) WriteEncryptedP(out io.Writer, alg x509.PEMCipher, pass []byte) {
	if err := k.WriteEncrypted(out, alg, pass); err != nil {
		panic(fmt.Errorf("failed to write key; %w", err))
	}
}

// Encode public portion of key as PEM according to its associated key format, then write it to a stream.
//
// Most private key formats defined in this library do not have a directly
// equivalent public key format. RSA+PKCS1 formats will encode public keys
// using PKCS1; all other formats use PKIX, a.k.a SubjectPublicKeyInfo.
func (k *Key) WritePublic(out io.Writer) error {
	blk, err := k.Format.encodePublic(k.Key)
	if err != nil {
		return err
	}
	return pem.Encode(out, blk)
}

// Alternate version of WritePublic that panics on error.
func (k *Key) WritePublicP(out io.Writer) {
	if err := k.WritePublic(out); err != nil {
		panic(fmt.Errorf("failed to write key; %w", err))
	}
}

// Encode private key as PEM according to its associated key format, then write it to a file.
func (k *Key) WriteFile(name string, mode os.FileMode) error {
	out, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, mode)
	if err != nil {
		return err
	}
	defer out.Close()
	return k.Write(out)
}

// Alternate version of WriteFile that panics on error.
func (k *Key) WriteFileP(name string, mode os.FileMode) {
	if err := k.WriteFile(name, mode); err != nil {
		panic(fmt.Errorf("failed to write key; %w", err))
	}
}

// Encode private key as passphrase-encrypted PEM according to its associated key format, then write it to a file.
func (k *Key) WriteEncryptedFile(name string, mode os.FileMode, alg x509.PEMCipher, pass []byte) error {
	out, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, mode)
	if err != nil {
		return err
	}
	defer out.Close()
	return k.WriteEncrypted(out, alg, pass)
}

// Alternate version of WriteEncryptedFile that panics on error.
func (k *Key) WriteEncryptedFileP(name string, mode os.FileMode, alg x509.PEMCipher, pass []byte) {
	if err := k.WriteEncryptedFile(name, mode, alg, pass); err != nil {
		panic(fmt.Errorf("failed to write key; %w", err))
	}
}

// Encode public portion of a key as PEM according to its associated key format, then write it to a file.
//
// Most private key formats defined in this library do not have a directly
// equivalent public key format. RSA+PKCS1 formats will encode public keys
// using PKCS1; all other formats use PKIX, a.k.a SubjectPublicKeyInfo.
func (k *Key) WritePublicFile(name string, mode os.FileMode) error {
	out, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, mode)
	if err != nil {
		return err
	}
	defer out.Close()
	return k.WritePublic(out)
}

// Alternate version of WritePublicFile that panics on error.
func (k *Key) WritePublicFileP(name string, mode os.FileMode) {
	if err := k.WritePublicFile(name, mode); err != nil {
		panic(fmt.Errorf("failed to write key; %w", err))
	}
}

// Implements crypto.PrivateKey.Equal; equality comparison between this private key and another.
func (k *Key) Equal(x crypto.PrivateKey) bool {
	if xk, ok := x.(*Key); ok {
		x = xk.Key
	}
	return k.Key.(interface {
		Equal(x crypto.PrivateKey) bool
	}).Equal(x)
}

// Implements crypto.PrivateKey.Public; returns the public key for this private key.
func (k *Key) Public() crypto.PublicKey {
	return k.Key.(interface {
		Public() crypto.PublicKey
	}).Public()
}

// Implements crypto.Signer.Sign; signs a digest with this private key.
func (k *Key) Sign(rand io.Reader, digest []byte, opts crypto.SignerOpts) (signature []byte, err error) {
	return k.Key.(crypto.Signer).Sign(rand, digest, opts)
}

func decodeKey(blk *pem.Block, pass []byte) (*Key, error) {
	var err error
	var der = blk.Bytes
	//lint:ignore SA1019 legacy formats needed for testing
	if blk.Type == "ENCRYPTED PRIVATE KEY" || x509.IsEncryptedPEMBlock(blk) {
		if pass == nil {
			return nil, fmt.Errorf("cannot decrypt private key without passphrase")
		}
		der, err = pemutil.DecryptPEMBlock(blk, pass)
		if err != nil {
			return nil, err
		}
	}
	var key Key
	switch blk.Type {
	case "RSA PRIVATE KEY":
		key.Key, err = x509.ParsePKCS1PrivateKey(der)
		key.Format = PKCS1
	case "ENCRYPTED PRIVATE KEY":
		fallthrough
	case "PRIVATE KEY":
		key.Key, err = x509.ParsePKCS8PrivateKey(der)
		key.Format = PKCS8
	case "EC PRIVATE KEY":
		key.Key, err = x509.ParseECPrivateKey(der)
		key.Format = SEC1
	default:
		return nil, fmt.Errorf("cannot decode %s as key", blk.Type)
	}
	if err != nil {
		return nil, err
	}
	kf, err := IdentifyKey(key.Key)
	if err != nil {
		return nil, err
	}
	key.Format |= kf
	return &key, nil
}
