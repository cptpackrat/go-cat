package cat

import (
	"encoding/hex"
	"math/big"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestParseTimeP(t *testing.T) {
	assert.Greater(t,
		ParseTimeP(time.RFC3339, "2020-01-01T00:00:02Z"),
		ParseTimeP(time.RFC3339, "2020-01-01T00:00:01Z"))
	assert.Panics(t, func() {
		ParseTimeP(time.RFC3339, "nah tho")
	})
}

func TestParseDurationP(t *testing.T) {
	assert.Greater(t, ParseDurationP("130s"), ParseDurationP("2m"))
	assert.Panics(t, func() {
		ParseDurationP("nah tho")
	})
}

func TestRandBytes(t *testing.T) {
	for _, l := range []int{4, 8, 32, 512} {
		b, err := RandBytes(l)
		assert.NoError(t, err)
		assert.Len(t, b, l)
		assert.True(t, isNotZero(b))
	}
}

func TestRandBytesP(t *testing.T) {
	for _, l := range []int{4, 8, 32, 512} {
		b := RandBytesP(l)
		assert.Len(t, b, l)
		assert.True(t, isNotZero(b))
	}
}

func TestRandHexBytes(t *testing.T) {
	for _, l := range []int{4, 8, 32, 512} {
		b, err := RandHexBytes(l)
		assert.NoError(t, err)
		assert.Len(t, b, hex.EncodedLen(l))
		assert.True(t, isHexAndNotZero(b))
	}
}

func TestRandHexBytesP(t *testing.T) {
	for _, l := range []int{4, 8, 32, 512} {
		b := RandHexBytesP(l)
		assert.Len(t, b, hex.EncodedLen(l))
		assert.True(t, isHexAndNotZero(b))
	}
}

func TestRandInt(t *testing.T) {
	t.Parallel()
	testRandInt(t, RandInt)
}

func TestRandIntP(t *testing.T) {
	t.Parallel()
	testRandInt(t, func(lsh uint) (*big.Int, error) {
		return RandIntP(lsh), nil
	})
}

func testRandInt(t *testing.T, fn func(lsh uint) (*big.Int, error)) {
	z := big.NewInt(0)
	for _, l := range []uint{32, 128, 512, 2048} {
		c := 0
		m := big.NewInt(1)
		h := big.NewInt(1)
		m.Lsh(m, l)
		h.Lsh(h, l-1)
		for i := 0; i < 1000; i++ {
			n, err := fn(l)
			assert.NoError(t, err)
			assert.Equal(t, 1, n.Cmp(z))
			assert.Equal(t, -1, n.Cmp(m))
			if h.Cmp(n) < 0 {
				c++
			}
		}
		assert.True(t, c > 350)
		assert.True(t, c < 650)
	}
}

func isNotZero(b []byte) bool {
	for _, v := range b {
		if v != 0 {
			return true
		}
	}
	return false
}

func isHexAndNotZero(b []byte) bool {
	d := make([]byte, hex.DecodedLen(len(b)))
	if _, err := hex.Decode(d, b); err != nil {
		return false
	}
	return isNotZero(d)
}
