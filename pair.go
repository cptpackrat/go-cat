package cat

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"
	"os"
)

// Represents a generated certificate with its associated private key.
type Pair struct {
	Key  *Key
	Cert *x509.Certificate
}

// Decode a PEM-encoded certificate pair and return it alongside any unconsumed bytes.
//
// Returns an error if the provided data does not contain either a PEM-encoded
// certificate, or a PEM-encoded private key followed by a certificate.
// If pass is not nil it will be used as a passphrase to attempt to
// decrypt private keys, otherwise encrypted keys will be rejected.
func DecodePair(data, pass []byte) (*Pair, []byte, error) {
	blk, rest := pem.Decode(data)
	if blk == nil {
		return nil, nil, fmt.Errorf("no certificate found")
	}
	return decodePair(blk, rest, pass)
}

// Alternate version of DecodePair that panics on error.
func DecodePairP(data, pass []byte) (*Pair, []byte) {
	pair, rest, err := DecodePair(data, pass)
	if err != nil {
		panic(fmt.Errorf("failed to decode pair; %w", err))
	}
	return pair, rest
}

// Read and decode a PEM-encoded certificate pair key from a file.
//
// Returns an error if the provided file does not contain either a PEM-encoded
// certificate, or a PEM-encoded private key followed by a certificate.
// If pass is not nil it will be used as a passphrase to attempt to
// decrypt private keys, otherwise encrypted keys will be rejected.
func ReadPairFile(name string, pass []byte) (*Pair, error) {
	data, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	pair, _, err := DecodePair(data, pass)
	return pair, err
}

// Alternate version of ReadPairFile that panics on error.
func ReadPairFileP(name string, pass []byte) *Pair {
	pair, err := ReadPairFile(name, pass)
	if err != nil {
		panic(fmt.Errorf("failed to read pair; %w", err))
	}
	return pair
}

// Encode certificate as PEM, optionally preceded by a plain-text copy of its private key.
func (p *Pair) Encode(withKey bool) ([]byte, error) {
	crt := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: p.Cert.Raw})
	if withKey {
		key, err := p.Key.Encode()
		if err != nil {
			return nil, err
		}
		return append(key, crt...), nil
	}
	return crt, nil
}

// Alternate version of Encode that panics on error.
func (p *Pair) EncodeP(withKey bool) []byte {
	pem, err := p.Encode(withKey)
	if err != nil {
		panic(fmt.Errorf("failed to encode pair; %w", err))
	}
	return pem
}

// Encode certificate as PEM, preceded by a passphrase-encrypted copy of its private key.
func (p *Pair) Encrypt(alg x509.PEMCipher, pass []byte) ([]byte, error) {
	key, err := p.Key.Encrypt(alg, pass)
	if err != nil {
		return nil, err
	}
	return append(key, pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: p.Cert.Raw})...), nil
}

// Alternate version of Encrypt that panics on error.
func (p *Pair) EncryptP(alg x509.PEMCipher, pass []byte) []byte {
	pem, err := p.Encrypt(alg, pass)
	if err != nil {
		panic(fmt.Errorf("failed to encode pair; %w", err))
	}
	return pem
}

// Encode certificate as PEM, optionally preceded by a plain-text copy of its private key, then write it to a stream.
func (p *Pair) Write(out io.Writer, withKey bool) error {
	if withKey {
		if err := p.Key.Write(out); err != nil {
			return err
		}
	}
	return pem.Encode(out, &pem.Block{Type: "CERTIFICATE", Bytes: p.Cert.Raw})
}

// Alternate version of Write that panics on error.
func (p *Pair) WriteP(out io.Writer, withKey bool) {
	if err := p.Write(out, withKey); err != nil {
		panic(fmt.Errorf("failed to write pair; %w", err))
	}
}

// Encode certificate as PEM, preceded by a passphrase-encrypted copy of its private key, then write it to a stream.
func (p *Pair) WriteEncrypted(out io.Writer, alg x509.PEMCipher, pass []byte) error {
	if err := p.Key.WriteEncrypted(out, alg, pass); err != nil {
		return err
	}
	return pem.Encode(out, &pem.Block{Type: "CERTIFICATE", Bytes: p.Cert.Raw})
}

// Alternate version of WriteEncrypted that panics on error.
func (p *Pair) WriteEncryptedP(out io.Writer, alg x509.PEMCipher, pass []byte) {
	if err := p.WriteEncrypted(out, alg, pass); err != nil {
		panic(fmt.Errorf("failed to write pair; %w", err))
	}
}

// Encode certificate as PEM, optionally preceded by a plain-text copy of its private key, then write it to a file.
func (p *Pair) WriteFile(name string, mode os.FileMode, withKey bool) error {
	out, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, mode)
	if err != nil {
		return err
	}
	defer out.Close()
	return p.Write(out, withKey)
}

// Alternate version of WriteFile that panics on error.
func (p *Pair) WriteFileP(name string, mode os.FileMode, withKey bool) {
	if err := p.WriteFile(name, mode, withKey); err != nil {
		panic(fmt.Errorf("failed to write pair; %w", err))
	}
}

// Encode certificate as PEM, preceded by a passphrase-encrypted copy of its private key, then write it to a file.
func (p *Pair) WriteEncryptedFile(name string, mode os.FileMode, alg x509.PEMCipher, pass []byte) error {
	out, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, mode)
	if err != nil {
		return err
	}
	defer out.Close()
	return p.WriteEncrypted(out, alg, pass)
}

// Alternate version of WriteEncryptedFile that panics on error.
func (p *Pair) WriteEncryptedFileP(name string, mode os.FileMode, alg x509.PEMCipher, pass []byte) {
	if err := p.WriteEncryptedFile(name, mode, alg, pass); err != nil {
		panic(fmt.Errorf("failed to write pair; %w", err))
	}
}

// Return a tls.Certificate instance created from this certifcate pair.
func (p *Pair) TLS() *tls.Certificate {
	return &tls.Certificate{
		Leaf:        p.Cert,
		PrivateKey:  p.Key,
		Certificate: [][]byte{p.Cert.Raw},
	}
}

// Return an x509.CertPool instance containing this certificate.
func (p *Pair) CertPool() *x509.CertPool {
	pool := x509.NewCertPool()
	pool.AddCert(p.Cert)
	return pool
}

// Returns true if this certificate is self-signed.
func (p *Pair) SelfSigned() bool {
	return p.Cert.CheckSignatureFrom(p.Cert) == nil && bytes.Equal(p.Cert.RawIssuer, p.Cert.RawSubject)
}

// Returns a string representation of this certificate pair.
func (p *Pair) String() string {
	if p.Key == nil {
		return fmt.Sprintf("Pair{Key=nil,Self=%v,Issuer='%s',Subject='%s'}", p.SelfSigned(), p.Cert.Issuer, p.Cert.Subject)
	}
	return fmt.Sprintf("Pair{Key=%s,Self=%v,Issuer='%s',Subject='%s'}", p.Key.Format, p.SelfSigned(), p.Cert.Issuer, p.Cert.Subject)
}

func decodePair(blk *pem.Block, rest, pass []byte) (*Pair, []byte, error) {
	var pair Pair
	var err error
	if blk.Type != "CERTIFICATE" {
		pair.Key, err = decodeKey(blk, pass)
		if err != nil {
			return nil, nil, err
		}
		blk, rest = pem.Decode(rest)
		if blk == nil {
			return nil, nil, fmt.Errorf("no certificate found")
		}
	}
	if blk.Type != "CERTIFICATE" {
		return nil, nil, fmt.Errorf("cannot decode %s as certificate", blk.Type)
	}
	pair.Cert, err = x509.ParseCertificate(blk.Bytes)
	if err != nil {
		return nil, nil, err
	}
	return &pair, rest, nil
}
