package cat

import (
	"bytes"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
)

type encodePairCase struct {
	name string
	fn   func(p *Pair, wk bool) ([]byte, error)
}

type decodePairCase struct {
	name     string
	withKey  bool
	withRest []byte
}

func (c encodePairCase) String() string { return c.name }
func (c decodePairCase) String() string { return c.name }

func TestDecodePair(t *testing.T) {
	t.Parallel()
	testDecodePair(t, true, DecodePair)
}

func TestDecodePairP(t *testing.T) {
	t.Parallel()
	p1 := GenerateChainP([]*Template{{
		KeyFormat:   ECDSA_P384_PKCS8,
		Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test"}},
	}})[0]
	p2, _ := DecodePairP(p1.EncodeP(true), nil)
	assert.True(t, p2.Key.Equal(p1.Key))
	assert.True(t, p2.Cert.Equal(p1.Cert))
	assert.Panics(t, func() {
		DecodePairP([]byte(""), nil)
	})
}

func TestReadPairFile(t *testing.T) {
	t.Parallel()
	testDecodePair(t, false, func(data, pass []byte) (*Pair, []byte, error) {
		tmp := path.Join(t.TempDir(), "test.pem")
		err := os.WriteFile(tmp, data, 0600)
		if err != nil {
			return nil, nil, err
		}
		pair, err := ReadPairFile(tmp, pass)
		return pair, nil, err
	})
}

func TestReadPairFileP(t *testing.T) {
	t.Parallel()
	tmp := path.Join(t.TempDir(), "test.pem")
	p1 := GenerateChainP([]*Template{{
		KeyFormat:   ECDSA_P384_PKCS8,
		Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test"}},
	}})[0]
	p1.WriteFileP(tmp, 0600, true)
	p2 := ReadPairFileP(tmp, nil)
	assert.True(t, p2.Key.Equal(p1.Key))
	assert.True(t, p2.Cert.Equal(p1.Cert))
	assert.Panics(t, func() {
		ReadPairFileP(tmp+".nope", nil)
	})
}

func testDecodePair(t *testing.T, withRest bool, fn func(data, pass []byte) (*Pair, []byte, error)) {
	testParallelForEach(t, []KeyFormat{
		RSA_2048_PKCS1,
		RSA_2048_PKCS8,
		ECDSA_P256_PKCS8,
		ECDSA_P256_SEC1,
		ED25519_PKCS8,
	}, func(t *testing.T, kf KeyFormat) {
		p1 := GenerateChainP([]*Template{{
			KeyFormat:   ECDSA_P384_PKCS8,
			Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test"}},
		}})[0]
		testParallelForEach(t, []decodePairCase{
			{"PairOnly", true, []byte("")},
			{"PairOnlyNoKey", false, []byte("")},
			{"MixedData", true, []byte("other data\n")},
			{"MixedDataNoKey", false, []byte("other data\n")},
		}, func(t *testing.T, c decodePairCase) {
			pem, err := p1.Encode(c.withKey)
			assert.NoError(t, err)
			if len(c.withRest) > 0 {
				pem = []byte(fmt.Sprintf("%s%s%s", c.withRest, pem, c.withRest))
			}
			p2, rest, err := fn(pem, nil)
			assert.NoError(t, err)
			assert.True(t, p2.Cert.Equal(p1.Cert))
			if c.withKey {
				assert.True(t, p2.Key.Equal(p1.Key))
			} else {
				assert.Nil(t, p2.Key)
			}
			if withRest {
				assert.Equal(t, c.withRest, rest)
			}
		})
		testParallel(t, "EncryptedKey", func(t *testing.T) {
			pem, err := p1.Encrypt(x509.PEMCipherAES256, []byte("password"))
			assert.NoError(t, err)
			_, _, err = fn(pem, nil)
			assert.ErrorContains(t, err, "cannot decrypt private key without passphrase")
			p2, _, err := fn(pem, []byte("password"))
			assert.NoError(t, err)
			assert.True(t, p2.Key.Equal(p1.Key))
			assert.True(t, p2.Cert.Equal(p1.Cert))
		})
	})
	testParallel(t, "NoPair", func(t *testing.T) {
		key := GenerateKeyP(ECDSA_P384_PKCS8).EncodeP()
		_, _, err := fn(key, nil)
		assert.ErrorContains(t, err, "no certificate found")
		_, _, err = fn([]byte(""), nil)
		assert.ErrorContains(t, err, "no certificate found")
		_, _, err = fn([]byte("other data"), nil)
		assert.ErrorContains(t, err, "no certificate found")
		_, _, err = fn([]byte("-----BEGIN NOT A KEY-----\nbm90IGEga2V5Cg==\n-----END NOT A KEY-----\n"), nil)
		assert.ErrorContains(t, err, "cannot decode NOT A KEY as key")
		_, _, err = fn([]byte(fmt.Sprintf("%s-----BEGIN NOT A CERT-----\nbm90IGEga2V5Cg==\n-----END NOT A CERT-----\n", key)), nil)
		assert.ErrorContains(t, err, "cannot decode NOT A CERT as certificate")
	})
}

func TestPair(t *testing.T) {
	t.Parallel()
	testParallelForEach(t, []KeyFormat{
		RSA_2048_PKCS1,
		RSA_3072_PKCS8,
		ECDSA_P256_SEC1,
		ECDSA_P384_PKCS8,
		ED25519_PKCS8,
	}, func(t *testing.T, kf KeyFormat) {
		ch, err := GenerateChain([]*Template{{
			KeyFormat: kf,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test"},
			},
		}})
		assert.NoError(t, err)
		assert.Len(t, ch, 1)
		testParallelForEach(t, []encodePairCase{
			{"Encode", func(p *Pair, withKey bool) ([]byte, error) {
				return p.Encode(withKey)
			}},
			{"Write", func(p *Pair, withKey bool) ([]byte, error) {
				buf := bytes.Buffer{}
				err := p.Write(&buf, withKey)
				return buf.Bytes(), err
			}},
			{"WriteFile", func(p *Pair, withKey bool) ([]byte, error) {
				tmp := path.Join(t.TempDir(), "test.key")
				err := p.WriteFile(tmp, 0600, withKey)
				if err != nil {
					return nil, err
				}
				return os.ReadFile(tmp)
			}},
		}, func(t *testing.T, c encodePairCase) {
			testParallel(t, "WithKey", func(t *testing.T) {
				raw, err := c.fn(ch[0], true)
				assert.NoError(t, err)
				blk, raw := pem.Decode(raw)
				assert.NotNil(t, blk)
				key, err := tryDecodeKey(t, kf, blk)
				assert.NoError(t, err)
				assert.True(t, ch[0].Key.Equal(key))
				blk, _ = pem.Decode(raw)
				assert.NotNil(t, blk)
				assert.Equal(t, "CERTIFICATE", blk.Type)
				crt, err := x509.ParseCertificate(blk.Bytes)
				assert.NoError(t, err)
				assert.True(t, ch[0].Cert.Equal(crt))
			})
			testParallel(t, "WithoutKey", func(t *testing.T) {
				raw, err := c.fn(ch[0], false)
				assert.NoError(t, err)
				blk, _ := pem.Decode(raw)
				assert.NotNil(t, blk)
				assert.Equal(t, "CERTIFICATE", blk.Type)
				crt, err := x509.ParseCertificate(blk.Bytes)
				assert.NoError(t, err)
				assert.True(t, ch[0].Cert.Equal(crt))
			})
		})
		testParallelForEach(t, []encodePairCase{
			{"Encrypt", func(p *Pair, _ bool) ([]byte, error) {
				return p.Encrypt(x509.PEMCipherAES256, []byte("password"))
			}},
			{"WriteEncrypted", func(p *Pair, _ bool) ([]byte, error) {
				buf := bytes.Buffer{}
				err := p.WriteEncrypted(&buf, x509.PEMCipherAES256, []byte("password"))
				return buf.Bytes(), err
			}},
			{"WriteEncryptedFile", func(p *Pair, _ bool) ([]byte, error) {
				tmp := path.Join(t.TempDir(), "test.key")
				err := p.WriteEncryptedFile(tmp, 0600, x509.PEMCipherAES256, []byte("password"))
				if err != nil {
					return nil, err
				}
				return os.ReadFile(tmp)
			}},
		}, func(t *testing.T, c encodePairCase) {
			raw, err := c.fn(ch[0], true)
			assert.NoError(t, err)
			blk, raw := pem.Decode(raw)
			assert.NotNil(t, blk)
			key, err := tryDecryptKey(t, kf, blk, []byte("password"))
			assert.NoError(t, err)
			assert.True(t, ch[0].Key.Equal(key))
			blk, _ = pem.Decode(raw)
			assert.NotNil(t, blk)
			assert.Equal(t, "CERTIFICATE", blk.Type)
			crt, err := x509.ParseCertificate(blk.Bytes)
			assert.NoError(t, err)
			assert.True(t, ch[0].Cert.Equal(crt))
		})
	})
	testParallelForEach(t, []encodePairCase{
		{"EncodeP", func(p *Pair, withKey bool) ([]byte, error) {
			return p.EncodeP(withKey), nil
		}},
		{"EncryptP", func(p *Pair, _ bool) ([]byte, error) {
			return p.EncryptP(x509.PEMCipherAES256, []byte("password")), nil
		}},
		{"WriteP", func(p *Pair, withKey bool) ([]byte, error) {
			buf := bytes.Buffer{}
			p.WriteP(&buf, withKey)
			return buf.Bytes(), nil
		}},
		{"WriteEncryptedP", func(p *Pair, _ bool) ([]byte, error) {
			buf := bytes.Buffer{}
			p.WriteEncryptedP(&buf, x509.PEMCipherAES256, []byte("password"))
			return buf.Bytes(), nil
		}},
		{"WriteFileP", func(p *Pair, withKey bool) ([]byte, error) {
			tmp := path.Join(t.TempDir(), "test.key")
			p.WriteFileP(tmp, 0600, withKey)
			return os.ReadFile(tmp)
		}},
		{"WriteEncryptedFileP", func(p *Pair, _ bool) ([]byte, error) {
			tmp := path.Join(t.TempDir(), "test.key")
			p.WriteEncryptedFileP(tmp, 0600, x509.PEMCipherAES256, []byte("password"))
			return os.ReadFile(tmp)
		}},
	}, func(t *testing.T, c encodePairCase) {
		ch, err := GenerateChain([]*Template{{
			KeyFormat: ECDSA_P384_PKCS8,
			Certificate: x509.Certificate{
				Subject: pkix.Name{CommonName: "Test"},
			},
		}})
		assert.NoError(t, err)
		assert.Len(t, ch, 1)
		pem, err := c.fn(ch[0], true)
		assert.NoError(t, err)
		assert.Greater(t, len(pem), 0)
		ch[0].Key.Format = ECDSA_P384
		assert.Panics(t, func() {
			c.fn(ch[0], true)
		})
	})
	{
		ch, err := GenerateChain([]*Template{{
			KeyFormat:   RSA_2048,
			Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test 1"}},
		}, {
			Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test 2"}},
		}, {
			Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test 3"}},
		}})
		testParallel(t, "TLS", func(t *testing.T) {
			crt := ch[0].TLS()
			assert.True(t, crt.Leaf.Equal(ch[0].Cert))
			assert.Len(t, crt.Certificate, 1)
			assert.Equal(t, crt.Certificate[0], ch[0].Cert.Raw)
		})
		testParallel(t, "CertPool", func(t *testing.T) {
			pool := ch[2].CertPool()
			//lint:ignore SA1019 subjects needed for testing
			subs := pool.Subjects()
			assert.Len(t, subs, 1)
			assert.Equal(t, subs[0], ch[2].Cert.RawSubject)
		})
		testParallel(t, "SelfSigned", func(t *testing.T) {
			assert.NoError(t, err)
			assert.Len(t, ch, 3)
			assert.False(t, ch[0].SelfSigned())
			assert.False(t, ch[1].SelfSigned())
			assert.True(t, ch[2].SelfSigned())
		})
	}
}
