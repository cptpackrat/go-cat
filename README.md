# go-cat

[![go reference](https://pkg.go.dev/static/frontend/badge/badge.svg)](https://pkg.go.dev/gitlab.com/cptpackrat/go-cat)
[![pipeline status](https://gitlab.com/cptpackrat/go-cat/badges/main/pipeline.svg)](https://gitlab.com/cptpackrat/go-cat/-/commits/main)
[![coverage report](https://gitlab.com/cptpackrat/go-cat/badges/main/coverage.svg)](https://gitlab.com/cptpackrat/go-cat/-/commits/main)

This package implements a set of shorthand/utility functions for performing CA tasks during testing, such as:
- generating private keys and passphrases,
- generating chains of one or more CA certificates,
- issuing server and client certificates from those chains,
- marshaling private keys to/from files/buffers with/without encryption,
- marshaling certificates and certificate chains to/from files/buffers.

Need to generate a self-signed server certificate pair with an RSA private
key, write it to a file, and don't want to deal with error handling?
```go
cat.GenerateServerChainP([]*cat.Template{{
  KeyFormat: cat.RSA_2048_PKCS8,
  Certificate: x509.Certificate{
    Subject:  pkix.Name{CommonName: "example.com"},
    DNSNames: []string{"example.com"},
  }
}}).WriteFileP("test.pem", 0600, true)
```

How about multiple versions of that same certificate pair, issued across
a range of private key algorithms and encodings?
```go
for _, kf := range cat.KeyFormat{
  cat.RSA_2048_PKCS1,
  cat.RSA_4096_PKCS8,
  cat.ECDSA_P384_SEC1,
  cat.ECDSA_P521_PKCS8,
  cat.ED25519_PKCS8
} {
  cat.GenerateServerChainP([]*cat.Template{{
    KeyFormat: kf,
    Certificate: x509.Certificate{
      Subject:  pkix.Name{CommonName: "example.com"},
      DNSNames: []string{"example.com"},
    }
  }}).WriteFileP(fmt.Sprintf("test_%s.pem", kf), 0600, true)
}
```

How about a both a client and a server certificate issued from the same CA chain,
with the root CA certificate, server chain, and client chain each written to separate
files, _and_ each one using a different private key algorithm, _and_ the client
private key encrypted?
```go
ch := cat.GenerateChainP([]*cat.Template{{
  KeyFormat: cat.RSA_2048_PKCS8,
  Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test Root"}},
}, {
  Certificate: x509.Certificate{Subject: pkix.Name{CommonName: "Test Intermediate"}},
})
ch[1:].WriteFileP("root.crt", 0600, false)
ch.IssueServerChainP([]*cat.Template{{
  KeyFormat: cat.ECDSA_P384_PKCS8,
  Certificate: x509.Certificate{
    Subject:  pkix.Name{CommonName: "example.com"},
    DNSNames: []string{"example.com"},
  }
}})[:2].WriteFileP("server.pem", 0600, true)
ch.IssueClientChainP([]*cat.Template{{
  KeyFormat: cat.ED25519_PKCS8,
  Certificate: x509.Certificate{
    Subject: pkix.Name{CommonName: "Test Client"},
  }
}})[:2].WriteEncryptedFileP("client.pem", 0600, x509.PEMCipherAES256, []byte("password"))
```

You get the idea.

If it looks overwrought, that's because it is. All of the functionality provided here already exists
in the Go standard libraries, this package exists purely as a shorthand for that functionality in
scenarios where the tasks being performed are not the most important part of the code you're writing,
e.g. dynamically generating test keys and certificates as part of unit testing something else.

The idea is to allow for as concise an expression of intent as possible, and to that end most
functions implemented in this package are provided in multiple flavours (including panic-on-error
variants for _everything_) to hopefully minimise the number of function calls required to accomplish
a task.